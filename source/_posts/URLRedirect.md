---
title: Url Redirect
date: 2019-03-31
tags:
  - Web
categories: 渗透测试
---

<font color=green>URL重定向/跳转漏洞在漏洞挖掘中算是比较容易发现的一种，这里简单介绍下其原理，着重说一些发现过程和绕过方式</font>
<!--more-->




### <font color=dark>成因</font>

常见方式

- META标签的refresh
- Javascript
- header

meta

```html
<meta http-equiv="refresh" content="0; url=url">
```

Javascript

```javascript
location.href="url";
```

header

```php
<?php
    header("Location:url");
?>
```

当上述栗子中的url未限制时就产生了URL重定向/跳转漏洞

### <font color=dark>发现</font>

- 在退出处常见URL跳转

  ![](URLRedirect/1.jpg)

- 利用burp的search，搜索=http 同时返回包为302的内容

  ![](URLRedirect/3.png)

  发包到repeater,略作修改

  ![](URLRedirect/4.png)

### <font color=dark>绕过</font>

一般来说URL跳转发现比较容易，但是利用起来比较难，因为许多会做一些防护

1.xip.io绕过

![](URLRedirect/5.jpg)

这里只要不是www就会强制返回到https://xue.xx.com/login

但是利用https://xue.xx.com.*.xip.io

![](URLRedirect/6.jpg)



2.使用@绕过

这里并不能采用xip.io的方式绕过

![](URLRedirect/8.jpg)

可以使用@但是如果@后的域名不在规定范围内同样不可跳转

![](URLRedirect/9.jpg)

这个时候可以在@前加上/，\或者?尝试下

![](URLRedirect/10.jpg)

3.http://后加反斜杠绕过

有些时候@和.xip.io都不能使

![](URLRedirect/11.jpg)

这个时候可以采用http://\的样式尝试绕过

![](URLRedirect/12.jpg)

4.某些加密

http://xx.xx.cn/?src=l4uLj8XQ0IiIiNGHlpOK0ZyQktCFl4qekYuWoM7IycfIztGXi5KT

这个链接会跳转到http://www.xilu.com/zhuanti_176871.html

将 **l4uLj8XQ0IiIiNGHlpOK0ZyQktCFl4qekYuWoM7IycfIztGXi5KT** base64解密

```
\x97\x8b\x8b\x8f\xc5\xd0\xd0\x88\x88\x88\xd1\x87\x96\x93\x8a\xd1\x9c\x90\x92\xd0\x85\x97\x8a\x9e\x91\x8b\x96\xa0\xce\xc8\xc9\xc7\xc8\xce\xd1\x97\x8b\x92\x93
```

推测与跳转的链接一一对应，推测出对应关系表

```
{'a':'9e', 'b':'9d', 'c':'9c', 'd':'9b', 'e':'9a','f':'99', 'g':'98', 'h':'97', 'i':'96', 'j':'95',
 'k':'94', 'l':'93', 'm':'92', 'n':'91', 'o':'90','p':'8f', 'q':'8e', 'r':'8d', 's':'8c', 't':'8b',
 'u':'8a', 'v':'89', 'w':'88', 'x':'87', 'y':'86','z':'85','.':'d1','/':'d0',':':'c5'}
```

![](URLRedirect/7.jpg)





> 我寻了半生的春天，你一笑便是了。

