---
title: 渗透信息记录
date: 2019-05-09
tags:
  - Python
categories: 开发
---

<font color=green>对渗透测试时的要点进行记录，目前仅为命令行形式，后续将采用HTML形式</font>
<!--more-->

### <font color=dark>前言</font>
在渗透测试时总是懒的记录下来一些信息，导致后续查找非常困难，所以想使用Python结合Mysql作出一个方便记录的小程序，如果采用Web应用展示的话会更加直观，所以后续会采用Flask，目前只有命令行的形式

### <font color=dark>介绍</font>
```cmd
Usage:
    sdomain baidu.com,GET INFO ABOUT DOMAIN
    adomain baidu.com,ADD DOMAIN INFO
    udomain baidu.com,UPDATE DOMAIN INFO
    ssdomain www.baidu.com,GET INFO ABOUT SUBDOMAIN
    asdomain test.baidu.com,ADD SUBDOMAIN INFO
    usdomain test.baidu.com,UPDATE SUBDOMAIN INFO
    use baidu.com,CHOOSE,ONE DOMAIN FOR LATER OPERATE
    show domain/subdomain,SHOW ALL DOMAINS OR ALL SUBDOMAIN ABOUT THE SELECTED DOMAIN
    help,SEE THE MESSAGE
    exit,EXIT
```
使用cmd模块，所以使用**python main.py**进入主程序后采用上述usage进行使用

采用MySql进行数据存储，所以首先要进行数据库配置
在**sqlhandle.py**文件中可进行如下配置
```python
HOST = 'localhost'
USER = 'root'
PWD  = ''
DBS  = 'attackinfo'
```
将上述信息修改为自身信息即可

使用时记得开启MySql

操作演示
```cmd
python main.py
Usage:
    sdomain baidu.com,GET INFO ABOUT DOMAIN
    adomain baidu.com,ADD DOMAIN INFO
    udomain baidu.com,UPDATE DOMAIN INFO
    ssdomain www.baidu.com,GET INFO ABOUT SUBDOMAIN
    asdomain test.baidu.com,ADD SUBDOMAIN INFO
    usdomain test.baidu.com,UPDATE SUBDOMAIN INFO
    use baidu.com,CHOOSE,ONE DOMAIN FOR LATER OPERATE
    show domain/subdomain,SHOW ALL DOMAINS OR ALL SUBDOMAIN ABOUT THE SELECTED DOMAIN
    help,SEE THE MESSAGE
    exit,EXIT
MYINFO>sdomain baidu.com # 查询域名 baidu.com 数据库中需要存在 baidu.com的信息，不存在会提示先进行插入操作
[-]No Such Domain,Insert First!
MYINFO>adomain baidu.com # 插入域名
输入子域名，用逗号隔开: www.baidu.com,test.baidu.com
输入邮箱，用逗号隔开: laa@qq.com,lala@baidu.com
输入手机号，用逗号隔开: 13333333333,13888888888
输入用户名，用逗号隔开: admin,baidu
[+]Insert Success!
MYINFO>sdomain baidu.com # 插入成功后再次进行查询
Domain:
        bai.com
SubDomains:
        www.baidu.com
        test.baidu.com
Emails:
        laa@qq.com
        lala@baidu.com
Phone:
        13333333333
        13888888888
MYINFO>ssdomain test.baidu.com # 查询子域名信息
Domain:
        test.baidu.com
Title:
        None
Infos:

Warn:

Vulnerability:

MYINFO>show domains # 显示所有域名
[+]Result:
        baidu.com
        baishi.com
        bai.com

MYINFO>use baishi.com # 选择域名，进行子域名操作
[+]Useful domain baishi.com

MYINFO>show subdomains # 显示当前域名下所有子域名
[+]Result:
        www.baishi.com
        a.baishi.com
        ccc.baishi.com
        q.baishi.com
        test.baishi.com
        t.baishi.com

MYINFO>asdomain lala.baishi.com # 插入子域名信息
输入网站标题: Web测试
输入泄漏的信息，例如robot.txt，用<x>隔开: robots.txt<x>1
输入可能存在的漏洞信息，用<x>隔开: 哈哈哈<x>SQL注入
输入确认存在的漏洞信息，用<x>隔开: /down?q=filename q任意文件下载<x>/d?a=1 aSQL注入
[+]Insert Success!
```
GITHUB地址:
https://github.com/No4l/MyTools/tree/master/usefulMessage

>我转头，看见你走来，在阳光里，于是笑容从我心里溢出。
