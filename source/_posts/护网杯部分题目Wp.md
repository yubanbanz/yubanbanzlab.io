---
title: 护网杯部分题目Wp
date: 2018-10-13
tags:
categories: CTF

---

还是太菜了，只做出了三道题

<!--more-->

## 迟来的签到题

下载文件后是一段字符

`AAoHAR1WX1VQVlNVU1VRUl5XXyMjI1FWJ1IjUCQnXlZWXyckXhs=`

尝试了下，可以base64解码，不过解出来是乱码

![](护网杯部分题目Wp/1.png)



题目提示XOR，flag一般格式为flag{}，所以先试下

`chr(ord(a[0]) ^ ord(a[len(a)-1]) ^ ord('f'))`

![](护网杯部分题目Wp/2.png)

可以得到flag了

![](护网杯部分题目Wp/3.png)

## FEZ

fez.py

```python
import os
def xor(a,b):
    assert len(a)==len(b)
    c=""
    for i in range(len(a)):
        c+=chr(ord(a[i])^ord(b[i]))
    return c
def f(x,k):
    return xor(xor(x,k),7)
def round(M,K):
    L=M[0:27]
    R=M[27:54]
    new_l=R
    new_r=xor(xor(R,L),K)
    return new_l+new_r
def fez(m,K):
    for i in K:
        m=round(m,i)
    return m

K=[]
for i in range(7):
    K.append(os.urandom(27))
m=open("flag","rb").read()
assert len(m)<54
m+=os.urandom(54-len(m))

test=os.urandom(54)
print test.encode("hex")
print fez(test,K).encode("hex")
print fez(m,K).encode("hex")
```

fez.log

```
c8b84d08e5a8e60a49578f387fff5a90e9e7c181734bf05be4f5403c9ea24a0b8741a329991637e11fa69019cd3b01d7c95b65f5abd5
5c3660c27cb9b3785a5ce06022e88bc831017e882d39475ea85d919ad9e5ac498f86c553216cab1f8f7468353d46ba8971efa9ca8c81
519ab6fc0e435da00516b844f8fe664bfe9445992f478dc650701739a11ffda5bbeb643159d7e8cd03a2104c798a1ca734b905ee6c76
```

方便描述，给fez.log的值赋给三个变量

```python
a = "c8b84d08e5a8e60a49578f387fff5a90e9e7c181734bf05be4f5403c9ea24a0b8741a329991637e11fa69019cd3b01d7c95b65f5abd5"
b = "5c3660c27cb9b3785a5ce06022e88bc831017e882d39475ea85d919ad9e5ac498f86c553216cab1f8f7468353d46ba8971efa9ca8c81"
c = "519ab6fc0e435da00516b844f8fe664bfe9445992f478dc650701739a11ffda5bbeb643159d7e8cd03a2104c798a1ca734b905ee6c76"
```

b,c都是fez函数得到的，分析看看fez函数，作用是将m分成两段L,R，前后异或处理后与i异或,得到new_R,而R则成了new_L,拼接成m，循环Len(K)次

易知K的长度为7，有Ri,Li,Ki

最后可以得到

```
L= R1^K2^K3^K5^K6

R = R1^K1^L1^k2^k4^K6^K7

```

结合a,b,c很轻松得到flag

![](护网杯部分题目Wp/4.png)

还剩下一部分思路差不多的

## easy_tornado 

打开题目，有个文件列表

![](护网杯部分题目Wp/5.png)

随便访问一个有两个参数，随便修改下

![](护网杯部分题目Wp/6.png)

试了下模板注入，可以，只不过是过滤了许多东西，hint里面有一个cookie_secret

用的是tornado,了解到其存在一个hander.settings里面有cookie_secret

![](护网杯部分题目Wp/7.png)

得到cookie_secret后加密得到签名，最后拿到flag