---
title: BlueCMS 代码审计
date:  2018-10-03
tags:
  - PHP
  - Web
categories: 代码审计
---

blueCMS是一款小众的CMS ,网上也有很多漏洞分析之类的文章，用来做代码审计入门练习还是不错的选择

<!--more-->

## 0x00 环境准备

- Apache + PHP
- BlueCMS v1.6 sp1 
- Mysql监控

## 0x01大体结构分析

### 入口文件与common函数

大致看了下index文件，果然入口文件很难发现漏洞，继续看两个common文件

![](BlueCMS-代码审计/common-fun.png)

因为是GBK编码的CMS所以此处可以被宽字节注入，此外就是IP未过滤

![](BlueCMS-代码审计/comip.png)

## 0x02 漏洞点测试

### IP

首先测试下IP，全局搜索getip,在comment发现getip并且未做处理就放入SQL中执行

![](BlueCMS-代码审计/ip.png)

进入comment页面，不知道为什么没有内容显示，只好换个方向，在common.inc文件中$online_ip是由getip得到的

![](BlueCMS-代码审计/ip2.png)

搜索$online_ip后在guest_book文件中发现

![](BlueCMS-代码审计/ip3.png)

测试下功能，确认可用

![](BlueCMS-代码审计/ip4.png)

构造payload X-Forwarded-For:',database())#

![](BlueCMS-代码审计/p4.png)



可以在留言中看到数据库名已经出现了

![](BlueCMS-代码审计/ip5.png)

### 数值型注入

在ad_js.php文件中可以发现，$ad_id变量受我们控制，而且插入SQL语句时没用单引号，这就使我们可以直接注入了

![](BlueCMS-代码审计/ad_js.png)

union select得到数据库名称

![](BlueCMS-代码审计/get.png)

### xss漏洞

在user.php文件的增加新闻处

![](BlueCMS-代码审计/xss.png)

content并没有用htmlspecialchars函数过滤，而是采用了filter_data函数

![](BlueCMS-代码审计/xss1.png)

只要关键字大写就可以绕过

创建一个用户后发表一个新闻，content内容为XSS PAYLOAD

![](BlueCMS-代码审计/xss3.png)

切换账号访问新闻页

![](BlueCMS-代码审计/xss5.png)

## 0x03 总结

第一次代码审计一个CMS，果然我还是太菜了，只能发现最基础的一些漏洞，不过慢慢来啦，急不来。