---
title: CNVD漏洞检测
date: 2019-04-01
tags:
  - Python
categories: 开发
---

<font color=green>使用Python获取CNVD漏洞信息，与Redis相结合，用于储存信息，每次运行检测是否有新的漏洞产生，有的话进行提醒</font>
<!--more-->

### 4/1
简单的将Python与Redis进行结合做出来的第一个版本
```python
# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import requests
import redis
import re

#constant
HOST = 'localhost'
PORT = 6379
PWD  = ''
URL = "http://www.cnvd.org.cn/flaw/list.htm?flag=true"
headers = {
    'User-Agent':' Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0'
}
Data = {'max':20,'offset':0}
R = redis.Redis(host=HOST,port=PORT,password=PWD)



res = requests.post(URL,data=Data,headers=headers)
text = bytes.decode(res.content)

infos = re.findall('</img> <a\s.+href="/flaw/show/(.*?)"?\s+title="(.*?)">',text)

for i in infos:
    info = i[0]+','+i[1]
    if R.sadd('CNVD',info):
        print('[*]New Info:',info)

```
PS:使用集合可能不是那么明智的选择

### 4/1
更新代码，采用json保存用户信息，同时增加保存成文件的功能
```python
# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import argparse
import requests
import redis
import json
import sys
import re
import os

#import user info
try:
    UserInfo = json.load(open('userinfo.json'))
except:
    print('[-]Load Json File Error!')
    exit()

#use redis
r = redis.Redis(host=UserInfo['Host'],port=UserInfo['Port'],password=UserInfo['Pwd'])
try:
    acount = r.scard('CNVD')
    print('[+]目前漏洞数为:',acount)
except Exception as e:
    print('[-]Redis',e)
    exit()

def scanCNVD():
    res = requests.post(UserInfo['Url'],data=UserInfo['Data'],headers=UserInfo['Headers'])
    text = bytes.decode(res.content)
    infos = re.findall('</img> <a\s.+href="/flaw/show/(.*?)"?\s+title="(.*?)">',text)
    for i in infos:
        info = i[0]+','+i[1]
        if r.sadd('CNVD',info):
            print('[*]New Info:',info)
    print('[*]Scan Finish.')
    r.save()

def writeFile():
    cont = [bytes.decode(i) for i in r.smembers('CNVD')]
    if os.path.isfile('CNVD.csv'):
        with open('CNVD.csv','a') as af:
            af.write('\n'.join(cont))
    else:
        with open('CNVD.csv','w') as wf:
            wf.write('编号,内容\n'+'\n'.join(cont))
    print('[+]Save In CNVD.csv.')

if __name__ == '__main__':
    parse = argparse.ArgumentParser(description="CNVD INFO.")
    parse.add_argument('--update','-u',action='store_true',help='Update CNVD INFO')
    parse.add_argument('--file','-f',action='store_true',help='Save CNVD INFO')
    args = parse.parse_args()
    try:
        sys.argv[1]
    except:
        parse.print_help()
        exit()
    if args.file:
        writeFile()
    if args.update:
        scanCNVD()

```
json
```json
{
	"Host":"localhost",
	"Port": 6379,
	"Pwd" :"",
	"Url" :"http://www.cnvd.org.cn/flaw/list.htm?flag=true",
	"Data":{"max":20,"offset":0},
	"Headers":{"User-Agent":"Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0"}
}

```


### Git
已上传至Github
https://github.com/No4l/MyTools/tree/master/CNVD


>海底月是天上月，眼前人是心上人。向来心是看客心，奈何人是剧中人。
