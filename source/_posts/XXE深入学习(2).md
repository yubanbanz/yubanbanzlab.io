---
title: XXE深入学习(2)
date:  2020-01-17
tags: 
  - XXE
categories: Web
---



上一篇文章

[https://noel.xin/2020/01/17/%E6%B7%B1%E5%85%A5%E5%AD%A6%E4%B9%A0XXE/](https://noel.xin/2020/01/17/深入学习XXE/)

中提到过上传xls等文件导致的XXE，终于找到一个可以算是案例的案例Orz



#### 工具

首先介绍一个工具

https://github.com/BuffaloWill/oxml_xxe

![image-20200117212225073](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgimage-20200117212225073.png)

正常工作后的界面如图所示，我们可以通过它创建多种类型的恶意文件，使用起来还是很方便哒

其中的Display可以把文件的XML部分展示出来

![image-20200117212512223](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgimage-20200117212512223.png)

当然，不想要下载工具还有一种方法

选中xls或doc文件右键解压

![image-20200117212635222](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgimage-20200117212635222.png)

还原的话就是所有文件再压缩起来就可以咯

Linux

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/img228001124033.png)

重新压缩

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/img228001247718.png)

#### 案例

在某个站存在一个上传会员数据的地方

![image-20200117213118341](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgimage-20200117213118341.png)

首先把模板下载下来打开看看

![image-20200117213316500](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgimage-20200117213316500.png)

在第四行填上信息后把xls文件解压

![image-20200117212635222](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgimage-20200117212635222.png)

找到我们填的数据在哪个文件里

![image-20200117213837652](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgimage-20200117213837652.png)

首先测试是否会解析，在文件头加入

```xml
<!DOCTYPE r [
<!ENTITY test "金卡" >
]>
```

将姓名和会员卡对应的数值修改为`&test;`

将修改过的文件打包成xlss文件进行上传

![image-20200117213956237](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgimage-20200117213956237.png)

提示失败，没关系，下载失败数据看看

![image-20200117214050245](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgimage-20200117214050245.png)

确实生效了，接下来测试

```xml
<!DOCTYPE r [
<!ENTITY test SYSTEM "file:///etc/passwd" >
]>
```

如果能够成功的话就会返回passwd的信息

然鹅，事实总是残酷的

![image-20200117214246256](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgimage-20200117214246256.png)

这次什么数据也没有，又尝试了http，还是没有任何信息，不禁让我想起了上次PHP的禁止外部DTD,再起不能Orz

![img](https://wiki-1258274765.cos.ap-nanjing.myqcloud.com/wiki/imgu=3659091698,1775640748&fm=26&gp=0.jpg)

参考链接:

https://medium.com/@jonathanbouman/xxe-at-bol-com-7d331186de54