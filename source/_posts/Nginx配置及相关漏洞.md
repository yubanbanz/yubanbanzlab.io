---
title: Nginx配置及相关漏洞
date: 2020-01-15
tags:
  - Web
  - Nginx
categories: 
  - 服务器
search:
  - nginx
---

nginx的基本配置以及相关漏洞

(依旧是前段时间的笔记)

<!--more -->

#### 安装

http://nginx.org/en/download.html

下载Nginx

找到conf/nginx.conf

```conf
location ~ \.php$ {

            root           D:/phpstudy_pro/WWW;# 网站目录
            fastcgi_pass   127.0.0.1:9000;#php-cgi监听端口号
            fastcgi_index  index.php;
            fastcgi_param  SCRIPT_FILENAME  D:/phpstudy_pro/WWW$fastcgi_script_name;
            include        fastcgi_params;
        }
```

在配置下php

找到php.ini

extension_dir修改为php的ext目录

```
extension_dir="D:\phpstudy_pro\Extensions\php\php5.6.9nts\ext"
```

使用php-cgi.exe

```cmd
Usage: php [-q] [-h] [-s] [-v] [-i] [-f <file>]
       php <file> [args...]
  -a               Run interactively
  -b <address:port>|<port> Bind Path for external FASTCGI Server mode
  -C               Do not chdir to the script's directory
  -c <path>|<file> Look for php.ini file in this directory
  -n               No php.ini file will be used
  -d foo[=bar]     Define INI entry foo with value 'bar'
  -e               Generate extended information for debugger/profiler
  -f <file>        Parse <file>.  Implies `-q'
  -h               This help
  -i               PHP information
  -l               Syntax check only (lint)
  -m               Show compiled in modules
  -q               Quiet-mode.  Suppress HTTP Header output.
  -s               Display colour syntax highlighted source.
  -v               Version number
  -w               Display source with stripped comments and whitespace.
  -z <file>        Load Zend extension <file>.
  -T <count>       Measure execution time of script repeated <count> times.
  
php-cgi.exe -b 127.0.0.1:9000 -c php.ini
```

运行php-cgi

使用nginx.exe

```cmd
nginx version: nginx/1.17.6
Usage: nginx [-?hvVtTq] [-s signal] [-c filename] [-p prefix] [-g directives]

Options:
  -?,-h         : this help
  -v            : show version and exit
  -V            : show version and configure options then exit
  -t            : test configuration and exit
  -T            : test configuration, dump it and exit
  -q            : suppress non-error messages during configuration testing
  -s signal     : send signal to a master process: stop, quit, reopen, reload
  -p prefix     : set prefix path (default: NONE)
  -c filename   : set configuration file (default: conf/nginx.conf)
  -g directives : set global directives out of configuration file
```

默认下使用conf/nginx.conf的配置信息

接下来访问http://localhost

![img](Nginx配置及相关漏洞/clipboard.png)

写一个hello world

![img](Nginx配置及相关漏洞/clipboard-1579017664685.png)

至此环境搭建完毕

#### **配置**

配置文件结构

```
...              #全局块

events {         #events块
   ...
}

http      #http块
{
    ...   #http全局块
    server        #server块
    { 
        ...       #server全局块
        location [PATTERN]   #location块
        {
            ...
        }
        location [PATTERN] 
        {
            ...
        }
    }
    server
    {
      ...
    }
    ...     #http全局块
}
```

更改配置文件后可使用

```cmd
nginx -t 配置文件
```

测试配置文件是否可用

```
D:\phpstudy_pro\Extensions\nginx-1.17.6>nginx.exe -t
nginx: the configuration file D:\phpstudy_pro\Extensions\nginx-1.17.6/conf/nginx.conf syntax is ok
nginx: configuration file D:\phpstudy_pro\Extensions\nginx-1.17.6/conf/nginx.conf test is successful
```

##### **开放不同端口**

```yaml
events {
    worker_connections  1024;
}
http {
    server {
        listen       80;
        server_name  localhost;

        location / {
            root   html;
            index  index.html index.htm;
        }

    }
    server {
        listen       81;
        server_name  localhost;

        location / {
            root   html;
            index  index.html index.htm;
        }
        
    }
}
```

PS:events 必须要有，这里的worker_connections表示最大连接数

##### **使用不同域名**

```
http {
    server {
    listen       80;
    server_name  noel.cn;
    root           D:\phpstudy_pro\WWW\seay;# 网站目录

    location / {
        index  index.html,index.htm,index.php;
    }
    }
    server {
        listen       80;
        server_name  noel.com;

        root   D:\phpstudy_pro\WWW;# 网站目录

        location / {
            index index.html,index.htm,index.php;
        }

    }
}
```

PS:本地需要修改hosts文件

也可以新建一个sites-enabled目录，在文件夹下新建nginx-com.conf

```
server {
    listen       80;
    server_name  noel.com;

    root   D:\phpstudy_pro\WWW;# 网站目录

    location / {
        index index.html,index.htm,index.php;
    }

}
```

nginx-cn.conf

```
server {
    listen       80;
    server_name  noel.cn;
    root           D:\phpstudy_pro\WWW\seay;# 网站目录

    location / {
        index  index.html,index.htm,index.php;
    }
}
```

此时的nginx.conf

```
events {
    worker_connections  1024;
}


http {
    include sites-enabled/nginx-*.conf;
}

```

##### **alias**

> This directive assigns a path to be used for the indicated location.

给指定的目录分配一个URL路径

在nginx-cn.conf文件的server中添加如下内容

```
    location /html/ {
        alias D:/phpstudy_pro/Extensions/nginx-1.17.6/html/;
    }
```

这时候我们访问的http://noel.cn/html/index.html中的index.html就是alias指定目录下的文件

##### 404

在nginx.conf的http模块中加入

```
error_page 404 https://www.baidu.com; #错误页
proxy_intercept_errors on;#如果被代理服务器返回的状态码为400或者大于400，设置的error_page配置起作用。默认为off。
```

意为将404页面跳转至百度



#### 漏洞

##### 目录遍历

在nginx.conf的http模块中加入

```
autoindex on;
```

![img](Nginx配置及相关漏洞/clipboard-1579019458766.png)

##### 目录穿越

前面我们有配置过alias,当alias时出现错误就可导致这一漏洞

这次在nginx-com.conf文件的server中添加如下内容

```
    location /html {
        alias D:/phpstudy_pro/Extensions/nginx-1.17.6/html/;
    }
```

对比上面的配置，我们只是在html后没加/，这也就导致了目录穿越，不过只能穿越至上层目录，配合目录遍历可以看到文件内容

http://noel.com/html../

![img](Nginx配置及相关漏洞/clipboard-1579019538086.png)

思考：如果能确认存在该漏洞但不存在目录遍历是否能通过爆破利用

##### 解析漏洞

问题出在cgi上

当php.ini中cgi.fix_pathinfo值设置为1(默认不开启)

之后在上传一个图片马

http://noel.com/seay/test.jpg/.php

![img](Nginx配置及相关漏洞/clipboard-1579019566008.png)