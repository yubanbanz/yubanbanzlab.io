---
title: Python数据库操作
date: 2019-05-08
tags:
  - SQL
  - Python
categories: Python

---
<font color=green>Python的数据库操作，主要介绍Mysql</font>

<!--more-->
### <font color=dark>安装</font>
不同的数据库需要安装不同的API模块，为了操作Mysql，我们首先需要安装pymysql
```Python
>>> import pymysql
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ModuleNotFoundError: No module named 'pymysql'
```
如果出现这种情况表明还不存在MySQLdb
安装方式
```cmd
pip install pymysql
```

*ps:python2用mysqldb*

### <font color=dark>使用</font>
首先需要把MySql服务打开，然后使用pymysql.connect进行连接
```python
>>> db = pymysql.connect('localhost','root','123456')#最后可以加上要链接的数据库
>>> db
<pymysql.connections.Connection object at 0x10804a908>
```
之后要有一个操作游标负责执行SQL操作，最后使用fetchone或者是fetchall获得结果
```python
>>> cursor.execute(sql)
1
>>> cursor = db.cursor()
>>> sql = 'select @@version'
>>> cursor.execute(sql)
1
>>> cursor.fetchone()
('8.0.16',)
```

### <font color=dark>注意</font>
在除了查询操作以外的其他操作中，执行了SQL语句后都需要进行commit，如果发生错误可以选择回滚，
例如:
```Python
sql = 'delete from users where id=1'
try:
  cursor.execute(sql)
  db.commit()
except:
  db.rollback()
```

>见到你之后，一切都不重要了
