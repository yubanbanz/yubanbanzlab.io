---
title: 想要写出来自己的东西~~
date: 2019-02-01
tags:
  - Fuzz
  - Python
categories: 开发
---

<font color=green>在家无事，于是乎想自己开发一款工具，方便自己使用，顺便提高下能力吧~</font>


<!--more-->

## X-Fuzz工具

### <font color=green>预期功能</font>

- 对目标网站目录进行爆破
- 对目标网站参数进行爆破
- 对目标网站子域名进行爆破
- 每次爆破后更新字典，出现频率高的字段，排名靠前

### <font color=green>使用技术</font>

- Python 主要使用Python
- Redis     用来储存字典
- 其他       可能用到的别的

### <font color=green>目的</font>

- 方便渗透测试
- 自我锻炼

### <font color=green>用时</font>

- 整个寒假
- 率先完成对目录爆破的部分

### <font color=green>后续</font>

- 尽量每天更新代码
- 每天还会有别的更新