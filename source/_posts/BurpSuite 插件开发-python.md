---
title: BurpSuite 插件开发 python篇
date: 2020-04-05
tags:
  - Web
  - Python
categories: 工具
---



最近看了一篇 FastJson 自动化检测的文章

https://www.w2n1ck.com/article/44/

个人感觉挺好使的，本想整一个SQL注入检测的插件，结果满地是坑，特此记录一下。

### 简单修改

有师傅再原文基础上做了获取ceye信息，从而直接判断漏洞是否存在的改动

https://github.com/uknowsec/BurpSuite-Extender-fastjson

原文中使用的是 `requests `但是不晓得为撒子我的 requests 一直爆错，只好用了 urllib2 进行下简单的修改

![image-20200405174931599](BurpSuite 插件开发-python/image-20200405174931599.png)

可能是网络的问题，对 ceye 的请求特别慢，最后结合 xray 的反连平台修改了下脚本

![image-20200405175202602](BurpSuite 插件开发-python/image-20200405175202602.png)

到这里其实已经可以用了，但是，这个检测流程是在 processHttpMessage 也就是请求发送过程中进行的，也就是说请求必须要等该流程结束了才能结束，那如果检测个SQL延时注入岂不是要等个几十秒，体验极差，所以还是用传统的scan吧！

### 深入改造

为了应对不知道会不会出现的延时危机，组织派出了 IScannerCheck 

用到的类和方法：

#### IScannerCheck

https://portswigger.net/burp/extender/api/burp/IScannerCheck.html

扩展可以实现此接口，然后调用 IBurpExtenderCallbacks.registerScannerCheck() 来注册自定义扫描程序检查。 执行扫描时，Burp会要求检查对基本请求执行主动或被动扫描，并报告已发现的所有扫描程序问题。

#### doActiveScan

主动扫描时会执行

#### doPassiveScan

被动扫描时执行

看文档介绍，被动扫描只用来分析请求而不会发出新的请求，所以我们在 doActiveScan 下编写代码

```python
def doActiveScan(self, baseRequestResponse, insertionPoint):
    # 获取请求包的数据 返回的是数组
    resquest = baseRequestResponse.getRequest()
    # 对请求进行解析
    analyzedRequest = self._helpers.analyzeRequest(resquest)
    request_header = analyzedRequest.getHeaders()
    # 获取请求的内容 其实并不需要这一步
    request_bodys = resquest[analyzedRequest.getBodyOffset():].tostring()
    # 获取域名以及路径
    request_host, request_Path = self.get_request_host(request_header)
    request_contentType = analyzedRequest.getContentType()
    ............
```

相较于原来的代码，也就是把 messageInfo 改为了 baseRequestResponse，二者本质上是相同的

#### 问题报告

到目前为止的结果都是在插件面板打印出来，很容易就会忽略，为此，可以加一个 IScanIssue

https://portswigger.net/burp/extender/api/burp/IScanIssue.html

看官方的实例也挺简单的

https://github.com/PortSwigger/example-scanner-checks/blob/master/python/CustomScannerChecks.py

增加如下内容

```python
#!/usr/bin/python
# -*- coding: utf-8 -*-

from burp import IScanIssue

#
# 实现IScanIssue来保存自定义扫描问题细节
#
class CustomScanIssue (IScanIssue):
    def __init__(self, httpService, url, httpMessages, name, detail, severity):
        self._httpService = httpService
        self._url = url
        self._httpMessages = httpMessages
        self._name = name
        self._detail = detail
        self._severity = severity

    def getUrl(self):
        return self._url

    def getIssueName(self):
        return self._name

    def getIssueType(self):
        return 0

    def getSeverity(self):
        return self._severity

    def getConfidence(self):
        return "Certain"

    def getIssueBackground(self):
        pass

    def getRemediationBackground(self):
        pass

    def getIssueDetail(self):
        return self._detail

    def getRemediationDetail(self):
        pass

    def getHttpMessages(self):
        return self._httpMessages

    def getHttpService(self):
        return self._httpService
```

检测到漏洞后 返回一个CustomScanIssue即可

顺带一提，原检测脚本是检测到 json 才会检测，如果想更全面的检测的话可以，判断后缀后只要不是图片之类的都进行检测，但是 doActiveScan 会对每一个 InsertionPoint 进行检测，而 fastjson 只需要一次检测即可，这个时候可以用

```
insertionPoint.getInsertionPointType()
```

判断插入点的类型，尽可能选择一个合适的点儿进行检测，让请求量变少

### 后续

参考文档和案例帮助很大，但是再检测SQL的路上还是有好多问题，请求是肯定会很多的，但是如何变的不是那么多是个问题啊/(ㄒoㄒ)/~~

