---
title: Navicat脱裤
date: 2019-01-31
tags:
  - Web
  - PHP
categories: Tips

---
<font color=green>看到有使用Navicat脱裤的，做下记录</font>
<!--more-->
### <font color=green>准备工作</font>
#### Navicat目录下的PHP文件
![](./使用navicat来脱裤吧!/1.png)
#### 将PHP文件放在web目录
打开后的效果
![](./使用navicat来脱裤吧!/2.png)
### <font color=green>正式开始</font>
在本地打开Navicat，新建连接里选择http,输入目标地址
![](./使用navicat来脱裤吧!/3.jpg)
在常规里主机名写localhost,在输入账号密码
![](./使用navicat来脱裤吧!/4.jpg)
即可快乐的脱裤了