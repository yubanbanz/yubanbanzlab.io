---
title: Vulnhub JJS-CTF
date:  2018-10-05
tags:
  - Web
  - Linux
categories: Challenge
---

Description: There are five flags on this machine. Try to find them. It takes 1.5 hour on average to find all flags. 

这个靶机中存在五个flag,平均1.5小时就可以找到它们。

<!--more-->

## 0x00 主机发现

使用Nmap进行扫描

```bash
nmap -sS 192.168.1.0/24
```

发现目标机，并且其开放了22，与80端口

![](Vulnhub-JJS-CTF/00.png)

## 0x01 Web

发现网站存在robots.txt文件

![](Vulnhub-JJS-CTF/01.png)

一番测试后发现只有admin_area页面和flag页面有内容，其他的也只有uploaded_files目录可以访问

admin_area

![admin_area](Vulnhub-JJS-CTF/02.png)

flag

![](Vulnhub-JJS-CTF/03.png)

拿到第一个flag

扫目录发现flag.txt是forbidden，先不管它，其它的也没有什么发现，又回到了admin_area界面，怎么看都感觉是掩耳盗铃一般，查看源代码， 果真有收获。

![](Vulnhub-JJS-CTF/04.png)

第二个flag与账号密码就拿到手了

登陆后是一个上传界面，随便上传个一句话，居然成功了

![](Vulnhub-JJS-CTF/05.png)

## 0x02 Linux

拿菜刀连接一下，连接成功

![](Vulnhub-JJS-CTF/06.png)

在HTML目录下有一个hint文件

![](Vulnhub-JJS-CTF/07.png)

得到第三个flag的同时有了下一个flag的线索，找到technawi用户的密码

使用命令

```bash
find / -user 'technawi' 2>/dev/null
```

找到所有属于technawi，且当前用户拥有权限的文件

众多文件中数这个credentials.txt最可疑

![](Vulnhub-JJS-CTF/08.png)

cat打开

![](Vulnhub-JJS-CTF/09.png)

第四个flag，以及technawi账户的密码就拿到手了，剩下的一个flag估计就在网站根目录下的flag.txt下了

前期端口扫描的时候知道了该主机存在SSH，所以用xshell连接，账号密码就是上面的，连接后

```bash
cat /var/www/html/flag.txt
```

![](Vulnhub-JJS-CTF/10.png)

完工

## 0x03 总结

这一个算是比较简单的题目，整个做下来基本上很流畅，Linux上的命令还不太熟悉，以后要多加练习。