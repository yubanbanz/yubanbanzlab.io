---
title: XSS的检测（1）
date:  2021-10-14
tags: 
  - XSS
categories: Web

---

为了深入学习下`JS` ，准备先了解下如何检测`XSS` ，总之就是这样🤡

### 检测思路

最好检测的是反射型`xss` ，简单归纳下可以分为这几类：

- 在标签的`innerText` 中
- 在标签的属性中
- 在`script` 中

最简单的检测当然是无过滤的情况下：

- `innerText` 的情况时`payload` 出现标签可以直接解析
- 在标签属性中时逃逸出引号后,`payload`将被视为当前标签的一个属性，或者是作为href` 之类的值，可以传入链接触发漏洞

`DOM`型的检测可以将`js` 代码转换为`AST` 语法书的，通过分析找到可控的输入点，并且跟随该点可以找到有效的输出来检测`XSS`，本篇内容主要是对反射型`XSS`的检测

### 反射型XSS检测

这里可以对`HTML` 进行分析，确认我们`payload` 出现的位置，当然，万能的`python` 提供了一个方便的`HTMLParser`供我们使用。我们可以创建一个类继承`HTMLParser` ,然后重写它的`handle_starttag`,`handle_data` 方法，在解析到标签以及标签内容时进行自定义处理。

##### payload出现在标签内容中

假设我们的`payload`是`<noel>` ，如果存在`XSS`，使用`HTMLParser` 解析`html` 的时候会发现有一个`noel` 标签，这也方便了我们进行检测

```python
def handle_starttag(self,tag,attrs):
  if tag == 'noel':
    xxxxx
```

##### payload出现在标签属性中

首先需要了解的是在标签属性中存在`xss`时有两种情况：

1. 可以逃逸出引号
2. 可控链接

先来看第一种情况，当逃逸出引号后剩下的`payload` 也会被当做当前标签的属性，所以这时的检测方式就是检测某个标签的属性中是否包含有`payload`的部分

```python
for attr in attrs:
  	if payload in attr[0]:
      xxxxxx
```

第二种情况也是和`attrs` 有关，只要满足`attr[0] in ['href','src','....']` 时大致上就可以确认

