---
title: Apk逆向分析工具 ---Apkatshu
date: 2019-02-02
tags:
  - Java
  - Tools
categories: 工具使用

---
<font color=green>无意中发现了Apkatshu这款软件，就代码而言还是比较简单的，鉴于本人有点儿懒，就花了点儿时间深入了解了下</font>
<!--more-->
### <font color=green>简介</font>
> Tool help to extract URLs, emails, up address, and juicy data from .apk files.
> Apkatshu : tool written in bash/python for extracting interesting 
> data from apk files and save them into spplited txt files . 
> The user can choose wich decompiler to use  JADX or APKTOOL
- Apkatshu用于提取逆向后的APK文件中的urls,emails等敏感信息
- Apkatshu采用**Bash**与**Python*编写
- Apkatshu需要结合**JADX**或者是**APKTOOL**这两款APK逆向工具使用
- https://github.com/0xpwny/Apkatshu

#### JADX
> Command line and GUI tools for produce Java source code from Android Dex and Apk files
- 项目地址:https://github.com/skylot/jadx
可以下载编译好的版本，也可以下载源代码，由于一直编译出错，最后我选择了编译好后的版本

- 使用方法:`jadx 目标文件 -d 输出文件夹`

#### APKTOOL
- 项目地址:https://ibotpeaches.github.io/Apktool/install/
![](Apkatshu/1.png)
- 使用方法:`APKTOOL d 目标文件 -o 输出文件夹 -p`

### <font color=green>使用</font>
由于我用的是**windows**,所以需要进行一些修改和配置
#### APKTOOL
因为windows并不支持shell文件，所以需要用到**Git Bash**
在Apkatshu.sh中通过
```shell
$(apktool d $Application -o $dist_dir -q)
```
调用apktool
所以需要在https://ibotpeaches.github.io/Apktool/install/ 下载Linux版本，并将其放入Git Bash的**/usr/bin/**目录下

之后通过
```shell
./Apkatshu.sh APKTOOL 目标文件
```
即可

#### JADX
在选择JADX的时候一直出错，最后先使用JADX将文件反编译，之后
```shell
python Apkatshu.py 反编译的目录 JADX
```
对反编译后的文件进行分析

