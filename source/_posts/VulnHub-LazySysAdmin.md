---
title: VulnHub-LazySysAdmin
date: 2018-10-5
tags: 
  - CTF
  - Web
  - Linux
categories: Challenge
---
主要考察信息收集
<!--more-->

## 0x01 前期试探

使用nmap进行扫描,发现存在如下端口
![](VulnHub-LazySysAdmin/01.png)

可以做出简单的分析

| 端口   | 作用  |
| ---- | -------- |
| 22   | SSH登陆  |
| 80   | Web应用  |
| 445  | 共享文件 |
| 3306 | MySql    |


使用御剑扫描下Web应用
![](VulnHub-LazySysAdmin/02.png)
存在PHPMyAdmin与WordPress，和robots.txt文件
robots.txt
![](VulnHub-LazySysAdmin/03.png)
访问后发现没有多大用处,phpmyadmin与wordpress没有账号密码也并不好突破，
不过在wordpress处获得了一个可能的账号
togie
![](VulnHub-LazySysAdmin/04.png)
## 0x02 突破点Find
因为其开放了445端口，所以用enum4linux尝试进行扫描
![](VulnHub-LazySysAdmin/05.png)
可以看到share是OK的


windows下获取共享资源
```bash
net use k: \\10.10.10.132\share$
```

linux下获取共享资源
```bash
mount -t cifs -o username='',password='' //10.10.10.132/share$ /mnt
```

![](VulnHub-LazySysAdmin/06.png)
对得到文件进行查看
在deets.txt文件中发现一个密码
![](VulnHub-LazySysAdmin/07.png)

再加上刚开始得到的一个可能的账号，首先尝试了下ssh连接
没想到直接成功了,切换到root账户，完成
![](VulnHub-LazySysAdmin/00.png)

## 0x03 Web渗透
### 0x01 WebShell
这样就直接拿到Shell了？多少有些无趣，看了下wordpress的config文件
找到了数据库的账号和密码
![](VulnHub-LazySysAdmin/08.png)
记得刚才扫的有phpmyadmin目录，登陆成功，不过权限太低了，只好放弃
![](VulnHub-LazySysAdmin/09.png)
尝试下登陆wordpress,没想到还是数据库的账号密码登陆上去了
在404界面写入反弹shell
![](VulnHub-LazySysAdmin/10.png)
得到WebShell
![](VulnHub-LazySysAdmin/11.png)
### 0x02 提权
开坑待填……

## 0x04 总结
总的来说是一个比较简单的challange,基本上只要掌握了enum4linux以及获取共享资源的方法就可以，
继续加油。