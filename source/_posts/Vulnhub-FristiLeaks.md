---
title: Vulnhub FristiLeaks
date:  2018-10-02
tags: 
	- Web
	- Linux
categories: Challenge
---
A small VM made for a Dutch informal hacker meetup called Fristileaks. Meant to be broken in a few hours without requiring debuggers, reverse engineering, etc..
<!--more-->
## 0x01 开始前的准备

### 环境下载

https://download.vulnhub.com/fristileaks/FristiLeaks_1.3.ova.torrent
https://download.vulnhub.com/fristileaks/FristiLeaks_1.3.ova

### 配置环境

VMware users will need to manually edit the VM's MAC address to: 08:00:27:A5:A6:76 
也就是把VMware的MAC地址调为08:00:27:A5:A6:76，之后开启虚拟机即可

## 0x02 扫描

首先使用Nmap进行扫描

![](Vulnhub-FristiLeaks/nmap.png)

只有80端口开启，nmap扫描下80端口

![](Vulnhub-FristiLeaks/80.png)

可以看到robots文件存在，并有三个目录存在

访问192.168.1.5，除了一张图片，基本上没有什么了

![](Vulnhub-FristiLeaks/calm.png)

robots.txt里的三个页面打开后是同一张图片，看来是毫无用处了，用御剑扫了下目录发现images文件夹,基本没用

## 0x03 获得Shell

最后回到主页的图片上来，fristi，题目好像也有这个，于是试了下fristi

![](Vulnhub-FristiLeaks/login.png)

弱口令失败，看下源代码想找找有没有什么东西

![](Vulnhub-FristiLeaks/tip1.png)

![](Vulnhub-FristiLeaks/tip2.png)

Great

先将base64解码，出现PNG字样，用base64转图片

![](Vulnhub-FristiLeaks/pwd1.png)

得到一串字符，尝试登陆：

正好第一处tip中有个名字，尝试登陆

进入上传页面

![](Vulnhub-FristiLeaks/upload.png)

在namp扫描时发现服务器为Apache Linux所以尝试下多重后缀名

![](Vulnhub-FristiLeaks/upshell.png)

上传成功，用菜刀连接

![](Vulnhub-FristiLeaks/shell.png)

## 0x04 提升权限

正在骑马赶来的路上......