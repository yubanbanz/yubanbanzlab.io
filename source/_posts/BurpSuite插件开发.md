---
title: BurpSuite 插件开发(上)
date: 2019-01-25
tags:
  - Web
  - Java
categories: 渗透测试




---

<font color=green>最近一直在挖洞，经常要用到BurpSuite，而Burp 上的一下插件不太好使，就想要自己开发一些插件，在网上看了一些资料，这里来写一个简单的插件吧！</font>

<!--more-->

## <font color=green>开始前的准备</font>

开发所需要的SDK 包都可以在 **Burp — Extender — APIs** 处导出

由于所有接口类都有一个 **package burp**所以需要建一个burp包，把所有接口类导入进去，然后新建**BurpExtender.java**,代码都写到里面

## <font color=green>本篇中用到的Burp API</font>

### IBurpExtender

这是一个**必须**要实现的接口

需要实现的方法

```
void registerExtenderCallbacks(IBurpExtenderCallbacks callbacks)
```

Demo

```java
package burp;
public class BurpExtender implements IBurpExtender{
    @Override
    public void registerExtenderCallbacks(final IBurpExtenderCallbacks callbacks){
        // TODO here
    }
}
```

### ITab

此接口用于自定义的标签页

```
	//实现 ITab 接口的 getTabCaption 方法
    // 此方法用于获取自定义标签的标题文本
	@Override
	public String getTabCaption() {
		// TODO Auto-generated method stub
		return "Burp 标签测试";
	}
	// 实现 ITab 接口的 getUiComponent 方法
	// Burp 调用此方法获取自定义标签页显示的组件
	@Override
	public Component getUiComponent() {
		// TODO Auto-generated method stub
		return jPanelMain;
	}
```

### IBurpExtenderCallbacks

 Burp Suite 利用此接口向扩展中传递了许多回调方法，这些回调方法可被用于在 Burp 中执行多个操作。当扩展被加载后，Burp 会调用**registerExtenderCallbacks()** 方法，并传递一个 **IBurpExtenderCallbacks** 的实例。

方法有很多，具体的就看文档咯

demo

```java
package burp;
public class BurpExtender implements IBurpExtender{
	@Override
	public void registerExtenderCallbacks(IBurpExtenderCallbacks callbacks){
		// TODO Auto-generated method stub
		callbacks.setExtensionName("BurpExtender");//设置扩展名为“BurpExtender”
	}
}
```

### IExtensionHelpers

此接口提供了很多常用的辅助方法，这些扩展可用于协助处理Burp扩展所产生的各种常见任务，扩展可以通过调用**IBurpExtenderCallbacks.getHelpers** 获得此接口的实例。

## <font color=green>Hello Burp</font>

```java
package burp;
//实现的类名必须为“BurpExtender”
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.PrintWriter;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

 // 所有的扩展必须实现IBurpExtender接口
public class BurpExtender implements IBurpExtender,ITab{
	
    public PrintWriter stdout;
    public IExtensionHelpers hps; //IExtensionHelpers提供了很多常用的辅助方法
    public IBurpExtenderCallbacks cbs;
    public JPanel jPanelMain;

	//实现 ITab 接口的 getTabCaption 方法
    // 此方法用于获取自定义标签的标题文本
	@Override
	public String getTabCaption() {
		// TODO Auto-generated method stub
		return "Burp 标签测试";
	}
	// 实现 ITab 接口的 getUiComponent 方法
	// Burp 调用此方法获取自定义标签页显示的组件
	@Override
	public Component getUiComponent() {
		// TODO Auto-generated method stub
		return jPanelMain;
	}
	//必须提供的默认构造器
	//在这里写主要代码噢
	@Override
	public void registerExtenderCallbacks(IBurpExtenderCallbacks callbacks) {
		// TODO Auto-generated method stub
		callbacks.setExtensionName("BurpExtender");//设置扩展名为“BurpExtender”
		this.hps = callbacks.getHelpers();//IBurpExtenderCallbacks.getHelpers() 获得此接口的实例。
		this.cbs = callbacks;
		//getStdout()此方法用于获取当前扩展的标准输出流。扩展应该将所有输出写入此流，允许Burp用户配置在UI中处理输出的方式。
		this.stdout = new PrintWriter(callbacks.getStdout(), true);
		this.stdout.println("hello burp!");//插件被加载的时候输出
		
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				jPanelMain = new JPanel();
				JButton jButton = new JButton("Hello Burp");
                jButton.addMouseListener(new MouseAdapter() {

                    @Override
                    public void mouseClicked(MouseEvent e){
                        stdout.println("Hello Burp");
                    }
                });
                // 将按钮添加到 主面板 jPanelMain 中. 
                jPanelMain.add(jButton);
                // 设置自定义组件并添加标签
                cbs.customizeUiComponent(jPanelMain);
                cbs.addSuiteTab(BurpExtender.this);
            }
							
		});
				
	}
	
}
```



