---
title: SUID提权
date: 2020-02-21
tags:
  - 提权
categories: Linux
---

Linux的SUID提权~

### SUID

suid是什么呢，通俗的理解为其他用户执行这个程序的时候可以用该程序所有者/组的权限

举个栗子

```c
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
    return system(argv[1]);
}
```

编译出两个文件

![image-20200221164148377](SUID提权/image-20200221164148377.png)

切换下用户，并且执行下命令

![image-20200221164327517](SUID提权/image-20200221164327517.png)

### SUID提权

个人理解如上图所示，某个文件可以执行命令，且有root的s标志，这个时候当我们用这个文件来执行命令时便可以达成提权的效果啦~



### 查找符合文件

```
find / -user root -perm -4000 -print 2>/dev/null
find / -perm -u=s -type f 2>/dev/null
find / -user root -perm -4000 -exec ls -ldb {} \; 

-user 指定用户
-perm -4000 匹配包含--s --- ---权限的文件
```







