---
title: Subdomain Collect Plan
date: 2019-03-21
tags:
  - Python
categories: 开发
---

<font color=green>咕咕咕</font>
<!--more-->
### 3/21
基础版本,利用百度API收集子域名
```python
# -*- coding: utf-8 -*-
#!/usr/bin/env python3
#Author: Noel
import json
import requests
import argparse

#API URL
BDURL = "http://ce.baidu.com/index/getRelatedSites?site_address="
URLS = set({})

def baidu(url):
    res = requests.get(BDURL+url)
    a = json.loads(res.text)
    for i in a['data']:
        URLS.add(i['domain'])

if __name__ == '__main__':
    #Parameter Processing
    parser = argparse.ArgumentParser(description='Collect Subdomain.')
    parser.add_argument('--domain','-d',required=True,help='target domain')
    parser.add_argument('--file','-f',help='save file name.default is domain.txt')
    args = parser.parse_args()

    #GET Subdomain
    baidu(args.domain)

    #Subdomain Processing
    print('[+]Collect Complete.')
    if args.file:
        with open(args.file,'w') as wf:
            wf.write('\n'.join(URLS))
        print('[+]File Saved in',args.file)
    else:
        with open('domain.txt','w') as wf:
            wf.write('\n'.join(URLS))
        print('[+]File Saved in domain.txt')

```
### 持续更新
咕咕咕